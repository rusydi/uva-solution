#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <sstream>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int table[101][101];
vector<string> v1, v2, ans;
int it;

void recon(int i, int j) {
    if (i == 0 || j == 0)
       return;
    else if (v1[i-1] == v2[j-1]) {
        if (it > 0) cout << " ";
        ans.push_back(v1[i-1]);
        recon(i-1, j-1);
    }
    else if (table[i-1][j] > table[i][j-1])
        recon(i-1, j);
    else
        recon(i, j-1);
}

void LCS(vector<string> v1, vector<string> v2) {

    memset(table, 0, sizeof(table));
    
    for(int i=1; i<=v1.size(); i++) {
        for (int j=1; j<=v2.size(); j++) {
            if (v1[i-1] == v2[j-1])
                table[i][j] = table[i-1][j-1] + 1;
            else
                table[i][j] = max(table[i-1][j], table[i][j-1]);
        }
    }

    ans.clear();
    it=0;
    recon(v1.size(), v2.size());

    for(int i=ans.size()-1; i>=0; i--) {
        if (i < ans.size()-1) cout << " ";
        cout << ans[i];
    }
    cout << endl;
}

int main() {
    string str;
    int ctr=0;

    while(cin >> str) {
        if (str == "#") ctr++;

        if (ctr == 0 && str != "#") v1.push_back(str);
        else if (ctr == 1 && str != "#") v2.push_back(str);
        else if (ctr == 2) {
            //processing LCS
            LCS(v1, v2);

            //cleanup variable
            ctr=0;
            v1.clear();
            v2.clear();
        }
    }

    return 0;
}

