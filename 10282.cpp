#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <string>
#include <sstream>

using namespace std;

map<string, string> DICT;

int main() {
    string line;

    DICT.clear();

    while(getline(cin, line) && !line.empty()) {
        stringstream ss;
        string engword;
        string forword;

        ss.clear();
        ss << line;

        ss >> engword;
        ss >> forword;

        DICT[forword] = engword;
    }

    string word;

    while(cin >> word) {
        if (DICT[word].empty())
            cout << "eh" << endl;
        else
            cout << DICT[word] << endl;
    }

    return 0;
}
