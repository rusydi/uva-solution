#include<iostream>
#include<queue>

using namespace std;

struct cube
{
    signed x, y, z;
    signed nsteps;

    cube() : x(0), y(0), z(0), nsteps(0) {}
    cube(signed _x, signed _y, signed _z, signed _nsteps=0) :
            x(_x), y(_y), z(_z), nsteps(_nsteps) {}

    inline bool operator==(const cube & o) const
    {
        return (x == o.x && y == o.y && z == o.z);
    }
};

int main()
{
    signed L, R, C;

    while(cin >> L >> R >> C && L && R && C)
    {
        cube S, E;
        char G[30][30][30] = {{{}}};
        bool visited[30][30][30] = {{{}}};

        queue<cube> Q;

        for(signed i = 0; i < L; ++i)
            for(signed j = 0; j < R; ++j)
                for(signed k = 0; k < C; ++k)
                {
                    cin >> G[i][j][k];
                    if (G[i][j][k] == 'S')
                    {
                        S.x = i; S.y = j; S.z = k;
                        S.nsteps = 0;
                    }
                    else if (G[i][j][k] == 'E')
                    {
                        E.x = i; E.y = j; E.z = k;
                        E.nsteps = -1;
                    }
                    else if (G[i][j][k] == '#')
                        visited[i][j][k] = true;
                }

        Q.push(S);
        while(!Q.empty())
        {
            cube T = Q.front();
            Q.pop();

            if (E == T)
            {
                E.nsteps = T.nsteps;
                break;
            }

            if (T.x != 0 && !visited[T.x-1][T.y][T.z])
            {
                Q.emplace(T.x-1, T.y, T.z, T.nsteps+1);
                visited[T.x-1][T.y][T.z] = true;
            }
            if (T.x != L-1 && !visited[T.x+1][T.y][T.z])
            {
                Q.emplace(T.x+1, T.y, T.z, T.nsteps+1);
                visited[T.x+1][T.y][T.z] = true;
            }
            if (T.y != 0 && !visited[T.x][T.y-1][T.z])
            {
                Q.emplace(T.x, T.y-1, T.z, T.nsteps+1);
                visited[T.x][T.y-1][T.z] = true;
            }
            if (T.y != R-1 && !visited[T.x][T.y+1][T.z])
            {
                Q.emplace(T.x, T.y+1, T.z, T.nsteps+1);
                visited[T.x][T.y+1][T.z] = true;
            }
            if (T.z != 0 && !visited[T.x][T.y][T.z-1])
            {
                Q.emplace(T.x, T.y, T.z-1, T.nsteps+1);
                visited[T.x][T.y][T.z-1] = true;
            }
            if (T.z != C-1 && !visited[T.x][T.y][T.z+1])
            {
                Q.emplace(T.x, T.y, T.z+1, T.nsteps+1);
                visited[T.x][T.y][T.z+1] = true;
            }
        }

        if (E.nsteps != -1)
            cout << "Escaped in " << E.nsteps << " minute(s)." << endl;
        else
            cout << "Trapped!" << endl;
    }
    return 0;
}
