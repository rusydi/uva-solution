
#include<iostream>
#include<iomanip>
#include<stdio.h>
#include<cstring>

using namespace std;

typedef unsigned long long ULL;

ULL prime_list[] = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97, 101};
ULL prime_counter[35];

void prime_factor(int n) {
    ULL i=0;

    while(n != 0) {
        if (n % prime_list[i] == 0) {
            n -= (n / prime_list[i]);
            prime_counter[i]++;
        }
        else {
            if (prime_list[i+1] > n)
                break;
            else
                i++;
        }
    }
}

int main() {
    ULL n=2;

    while(n <= 100) {
        memset(&prime_counter, 0, sizeof(prime_counter));

        for(int i=2; i<=n; i++)
            prime_factor(i);

        cout << setw(3) << fixed << n <<"! =";

        ULL i=0;
        while(prime_counter[i] != 0) {
            if (i > 0 && i % 15 == 0) {
                cout << endl << "      ";
            }
            cout << setw(3) << fixed << prime_counter[i];
            i++;
        }
        cout << endl;
        n++;
    }
}
