#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <sstream>
#include <map>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    char key; cin >> key;
    string line;
    map<string, vector<int> > M;
    int linenum=1;
    cin.ignore();
    while(getline(cin, line)) {
        stringstream ss; ss << line;
        string word;

        while(ss >> word) {
            if (word[word.length() - 1] == '.' || word[word.length() - 1] == ',')
                word.erase(word.length()-1);

            if (word[0] == key && !binary_search(M[word].begin(), M[word].end(), linenum))
                M[word].push_back(linenum);
        }

        linenum++;
    }

    for(map<string, vector<int> >::iterator it = M.begin(); it != M.end(); it++) {
        cout << it->first;
        for(int i=0; i<it->second.size(); i++)
            cout << " " << it->second[i];
        cout << endl;
    }

    return 0;
}

