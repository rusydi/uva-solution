#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

vector<ULL> factorial;
ULL minbound, maxbound;

void init() {
    bool minboundfound = false;

    factorial.push_back(1);
    ULL i=0;

    while(factorial[i] < 6227020800) {
        i++;
        factorial.push_back(i*factorial[i-1]);

        if (factorial[i] > 10000 && !minboundfound) {
            minbound = i;
            minboundfound = true;
        }

        if (factorial[i] == 6227020800)
            maxbound = i;
    }
}

int main() {
    init();
    long long N;

    while(cin >> N) {

        if (N < 0) {
            if (N % 2 == 0)
                cout << "Underflow!" << endl;
            else
                cout << "Overflow!" << endl;
        }
        else if (N < minbound)
            cout << "Underflow!" << endl;
        else if (N > maxbound)
            cout << "Overflow!" << endl;
        else cout << factorial[N] << endl;
    }
    return 0;
}

