#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

int main() {
    vector<string> vstr;
    string str;

    while(getline(cin, str)) {
        stringstream ss;
        string temp;
        bool stat = false;

        vstr.clear();

        ss << str;

        for(int i=0; ss >> temp; i++) {

            if (stat)
                cout << " ";
            for(int j=temp.length() - 1; j>=0; j--) {
                cout << temp[j];
            }
            stat = true;
            
        }
        cout << endl;
    }
    
    return 0;
}
