#include<iostream>
#include<iomanip>
#include<map>

using namespace std;

int main()
{
    signed T;
    cin >> T;

    cin.ignore();
    cin.ignore();
    while(T--)
    {
        map<string, float> M;
        float total = 0.0;

        while(true)
        {
            string s;
            getline(cin, s);

            if (s.empty())
                break;

            M[s] += 1.0;
            total += 1.0;
        }

        for(const auto & entry : M)
        {
            float value = entry.second / total * 100;
            cout << entry.first << " " << fixed << setprecision(4) << value << endl;
        }
        if (T) cout << endl;
    }
    return 0;
}
