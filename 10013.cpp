#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

int num1[1000010];
int num2[1000010];
int result[1000010];

int main() {
    int testcase;

    cin >> testcase;

    for(int p=0; p<testcase; p++) {
        //int result;

        memset(&num1, 0, sizeof(num1));
        memset(&num2, 0, sizeof(num2));
        memset(&result, 0, sizeof(result));

        int numdigit;
        cin >> numdigit;

        for(int i=numdigit-1; i>=0; i--) {
            cin >> num1[i] >> num2[i];
        }

        int carry=0;
        for(int i=0; i<numdigit; i++) {
            if ( (result[i] = carry + num1[i] + num2[i]) > 9 )
                carry = 1;
            else
                carry = 0;

            result[i] %= 10;
        }

        for(int i=numdigit-1; i>=0; i--)
            cout << result[i];
        cout << endl << endl;
    }

    return 0;
}
