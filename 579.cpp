#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<cassert>
#include<climits>
#include<iomanip>
using namespace std;

#define REP(i,n) for(int i=0, _e(n); i<_e; i++)
#define FOR(i,a,b) for(int i(a), _e(b); i<=_e; i++)
#define FORD(i,a,b) for(int i(a), _e(b); i>=_e; i--) 
#define FORIT(i, m) for (__typeof((m).begin()) i=(m).begin(); i!=(m).end(); ++i)
#define SET(t,v) memset((t), (v), sizeof(t))
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize( unique( ALL(c) ) - (c).begin() )

#define sz size()
#define pb push_back
#define VI vector<int>
#define VS vector<string>

typedef long long LL;
typedef long double LD;
typedef pair<int,int> pii;

#define D(x) if(1) cout << __LINE__ <<" "<< #x " = " << (x) << endl;
#define D2(x,y) if(1) cout << __LINE__ <<" "<< #x " = " << (x) \
     <<", " << #y " = " << (y) << endl;

double str2double(string s) {
	double ans = 0.0;
	reverse(s.begin(), s.end());
	REP(i, s.length()) {
		ans += (s[i] - '0') * pow(10, i);
	}
	return ans;
}

int main() {
	string clock;
	while(getline(cin, clock) && clock != "0:00") {
		int separator =  clock.find(':');
		double h, m;

		h = str2double(clock.substr(0, separator));
		m = str2double(clock.substr(separator+1, clock.length()));

		double d = abs(30*h + m/2 - 6*m);

		if (d > 180) d = 360-d;
		cout << fixed;
		cout << setprecision(3) << d << endl;
	}
	return 0;
}
