#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<fstream>
#include<numeric>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<iterator>

using namespace std;

int gcd(int a, int b) {
    if (b == 0) return a;
    return gcd(b, a%b);
}

int main() {
    int N; cin >> N; cin.ignore();
    while(N--) {
        string line;
        stringstream ss;
        int temp, maxgcd=0;
        vector<int> v;
        
        getline(cin, line);
        ss << line;

        while(ss >> temp) v.push_back(temp);

        for(int i=0; i<v.size()-1; i++) {
            for(int j=i+1; j<v.size(); j++) {
                int currgcd = gcd(v[i], v[j]);
                if (currgcd > maxgcd) maxgcd = currgcd;
            }
        }

        printf("%d\n", maxgcd);
    }

    return 0;
}

