#include<algorithm>
#include<iostream>
#include<vector>

using namespace std;

int main()
{
    signed K;
    cin >> K;

    while(K--) {
        vector<signed> S;
        size_t M, N;
        cin >> M >> N;

        vector<signed> A(M);
        vector<signed> u(N);
        signed counter = -1;

        for(size_t i = 0; i < M; cin >> A[i], ++i);
        for(size_t i = 0; i < N; cin >> u[i], ++i);

        signed iA = 0;
        for(size_t iu = 0; iu < N; ++iu)
        {
            if (iA < u[ iu ])
                for(; iA < u[ iu ]; ++iA)
                    S.insert(lower_bound(S.begin(), S.end(), A[iA]), A[iA]);
            counter++;
            cout << S[counter] << endl;
        }
        if (K) cout << endl;
    }

    return 0;
}
