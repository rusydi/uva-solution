#include<iostream>
#include<map>
#include<set>
#include<vector>

using namespace std;

int main()
{
    unsigned n;
    while(cin >> n && n)
    {
        map<set<unsigned>, unsigned> M;

        for(unsigned i = 0; i < n; ++i)
        {
            vector<unsigned> courses(5);
            cin >> courses[0] >> courses[1] >> courses[2] >> courses[3] >> courses[4];
            set<unsigned> S(courses.begin(), courses.end());
            M[S]++;
        }

        unsigned ans = 0;
        unsigned maxpop = 0;
        for(auto it = M.begin(); it != M.end(); ++it)
        {
            if ((*it).second > maxpop)
            {
                ans = (*it).second;
                maxpop = (*it).second;
            }
            else if ((*it).second == maxpop)
            {
                ans += (*it).second;
            }
        }
        cout << ans << endl;
    }
}
