#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <sstream>

using namespace std;

typedef unsigned long long ULL;

bool is_palindrome(ULL num) {
    stringstream ss;
    string str1, str2;

    ss << num;
    ss >> str1;
    str2 = str1;
    reverse(str2.begin(), str2.end());

    if(str1 == str2)
        return true;
    else
        return false;
}

int main() {
    int tcase; cin >> tcase;

    while(tcase--) {
        ULL num;
        ULL c=0;

        cin >> num;


        while(!is_palindrome(num)) {
            stringstream ss;
            ULL revnum;
            string revstr;
            string tempstr;

            ss << num;
            ss >> tempstr;

            revstr = tempstr;
            reverse(revstr.begin(), revstr.end());

            ss.clear();
            ss << revstr;
            ss >> revnum;

            num = num + revnum;
            c++;
        }

        cout << c << " " << num << endl;
    }

    return 0;
}
