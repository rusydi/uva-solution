#include <iostream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <cstdlib>

using namespace std;

int main(int argc, char** argv) {

    char rot_table[110][110];
    memset(&rot_table, 0, sizeof(rot_table));

    for (int i=0; i<110; i++)
        for (int j=0; j<110; j++)
            rot_table[i][j] = ' ';

    string str;
    int c = 0;
    int maxlen = 0;

    while(getline(cin, str)) {
        for (int i=0; i<str.length(); i++)
            rot_table[i][c] = str[i];

        c++;

        if (str.length() > maxlen)
            maxlen = str.length();
    }

    for (int i=0; i<maxlen; i++) {
        for (int j=c-1; j>=0; j--)
            cout << rot_table[i][j];
        cout << endl;
    }
    return 0;
}
