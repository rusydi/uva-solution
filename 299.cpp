/*
 ID: rusydi.1
 PROG: test
 LANG: C++
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <string>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    int T; cin >> T;

    while(T--) {
        int L; cin >> L;
        int N[51];
        int ans=0;

        for(int i=0; i<L; i++)
            cin >> N[i];

        for(int i=0; i<L; i++) {
            for(int j=i+1; j<L; j++) {
                if (N[i] > N[j]) {
                    int temp = N[i];
                    N[i] = N[j];
                    N[j] = temp;
                    ans++;
                }
            }
        }
        cout << "Optimal train swapping takes "<< ans << " swaps." << endl;
    }

    return 0;
}

