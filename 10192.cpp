#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int table[101][101];

int LCS(string a, string b) {
    memset(table, 0, sizeof(table));

    for(int i=1; i<=a.length(); i++) {
        for(int j=1; j<=b.length(); j++) {
            if (a[i-1] == b[j-1])
                table[i][j] = table[i-1][j-1] + 1;
            else
                table[i][j] = max(table[i][j-1], table[i-1][j]);
        }
    }

    return table[a.length()][b.length()];
}

int main() {
    int t=0;
    string line1, line2;

    while(getline(cin, line1) && line1 != "#") {
        getline(cin, line2);

        cout << "Case #" << ++t << ": you can visit at most " << LCS(line1, line2)
                << " cities." << endl;
    }

    return 0;
}

