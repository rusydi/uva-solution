#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int table[101][101];

int LCS(vector<int> v1, vector<int> v2) {
    memset(table, 0, sizeof(table));

    for(int i=1; i<=v1.size(); i++) {
        for(int j=1; j<=v2.size(); j++) {
            if (v1[i-1] == v2[j-1])
                table[i][j] = table[i-1][j-1] + 1;
            else
                table[i][j] = max(table[i][j-1], table[i-1][j]);
        }
    }

    return table[v1.size()][v2.size()];
}

int main() {
    int M, N;
    int t=0;
    
    while(cin >> M >> N && M && N) {
        vector<int> v1, v2;

        for(int i=0; i<M; i++) {
            int num; cin >> num;
            v1.push_back(num);
        }
        for(int i=0; i<N; i++) {
            int num; cin >> num;
            v2.push_back(num);
        }
        cout << "Twin Towers #" << t+1 << endl;
        cout << "Number of Tiles : " << LCS(v1, v2) << endl << endl;

        t++;
    }

    return 0;
}

