#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <map>
#include <cmath>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

#define MAX 2001

bool isnprime[MAX];

void sieve() {
    isnprime[0] = isnprime[1] = true;

    for(int i=2; i<=sqrt(MAX); i++) {
        if (!isnprime[i]) {
            for(int j=i*i; j<MAX; j+=i)
                isnprime[j] = true;
        }
    }
}

int main() {
    sieve();
    int T; cin >> T;
    for(int t=0; t<T; t++) {
        string line;
        string ans;
        map<char, int> M;
        
        cin >> line;
        for(int i=0; i<line.length(); i++)
            M[line[i]]++;

        for(map<char, int>::iterator it=M.begin(); it!=M.end(); it++) {
            if (!isnprime[M[it->first]])
                ans += it->first;
        }

        cout << "Case " << t+1 << ": ";
        if (ans.empty())
            cout << "empty" << endl;
        else
            cout << ans << endl;
    }

    return 0;
}

