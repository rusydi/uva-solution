#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

ULL bigmod(ULL b, ULL e, ULL m) {
    if (e == 0)
        return 1;

    if (e % 2 == 0) {
        ULL t = bigmod(b, e/2, m);
        return (t*t % m);
    }
    else {
        ULL t = bigmod(b, (e-1)/2, m);
        return (b*(t*t % m) % m);
    }
}

int main() {
    ULL a,b,c;

    while(cin >> a >> b >> c) {
        ULL res = bigmod(a, b, c);
        cout << res << endl;
    }

    return 0;
}
