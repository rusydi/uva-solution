#include <iostream>
#include <string>

using namespace std;

int main() {
    int testcase;
    int casenum = 1;

    cin >> testcase;
    cin.ignore();

    for (int q=0; q<testcase; q++) {
        string text;

        int counter = 0;

        getline(cin, text);

        for (int i=0; i<text.length(); i++) {
            if (text[i] >= 'a' && text[i] <='o')
                counter += ((text[i] - 'a') % 3) + 1;

            else if (text[i] >= 'p' && text[i] <= 's')
                counter += (text[i] - 'p') + 1;

            else if (text[i] >= 't' && text[i] <= 'v')
                counter += (text[i] - 't') + 1;

            else if (text[i] >= 'w' && text[i] <= 'z')
                counter += (text[i] - 'w') + 1;

            else if (text[i] == ' ')
                    counter += 1;
        }

        cout << "Case #" << casenum << ": " << counter << endl;
        casenum++;
    }
}