#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    ULL R, N;
    int c=0;

    while(cin >> R >> N && R && N) {
        bool stat=false;
        int i=0;

        for(i=0; i<=26; i++) {
            if ((i+1)*N >= R) {
                stat=true;
                break;
            }
        }
        cout << "Case " << c+1 << ": ";
        if (stat)
            cout << i << endl;
        else
            cout << "impossible" << endl;
        c++;
    }
    return 0;
}
