#include<iostream>

using namespace std;

int main()
{
    int32_t n;
    while(cin >> n)
    {
        int32_t ans = __builtin_bswap32(n);
        cout << n <<  " converts to " << ans << endl;
    }
    return 0;
}
