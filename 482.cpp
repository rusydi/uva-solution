#include<algorithm>
#include<iostream>
#include<sstream>
#include<vector>

using namespace std;

int main()
{
    signed T;
    cin >> T;

    cin.ignore();
    while(T--)
    {
        cin.ignore();
        vector<pair<unsigned, string>> v;
        stringstream ss;
        string line;

        getline(cin, line);

        ss << line;

        unsigned index;
        while(ss >> index)
            v.emplace_back(make_pair(index, ""));
        for(size_t i = 0; i < v.size(); ++i)
            cin >> v[i].second;

        sort(v.begin(), v.end());

        for(const auto &val : v)
            cout << val.second << endl;
        if (T != 0)
            cout << endl;
        cin.ignore();
    }
    return 0;
}
