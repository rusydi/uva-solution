#include<iostream>
#include<cmath>
#include<cstring>

using namespace std;

int row[9];
int linectr;
int a, b;

bool is_valid(int tryrow, int col)
{
    for(int prevcol = 1; prevcol < col; ++prevcol)
        if (row[prevcol] == tryrow || abs(row[prevcol] - tryrow) == abs(prevcol - col))
            return false;
    return true;
}

void backtrack(int col)
{
    for(int tryrow=1; tryrow <= 8; ++tryrow)
    {
        if (col == b && tryrow != a) continue; //early prune

        if (is_valid(tryrow, col))
        {
            row[col] = tryrow;
            if ((col == 8) && (row[b] == a))
            {
                printf("%2d     ", ++linectr);
                for(int j=1; j <= 8; ++j) printf(" %d", row[j]);
                printf("\n");
            }
            else
                backtrack(col + 1);
        }
    }
}

int main()
{
    int T; cin >> T;
    while(T--)
    {
        linectr = 0;
        memset(row, 0, sizeof(int)*9);

        cin >> a >> b;
        cout << "SOLN       COLUMN" << endl;
        cout << " #      1 2 3 4 5 6 7 8" << endl << endl;
        backtrack(1);

        if (T) cout << endl;
    }
    return 0;
}
