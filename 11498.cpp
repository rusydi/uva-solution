#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<cassert>
#include<climits>
using namespace std;

#define REP(i,n) for(int i=0, _e(n); i<_e; i++)
#define FOR(i,a,b) for(int i(a), _e(b); i<=_e; i++)
#define FORD(i,a,b) for(int i(a), _e(b); i>=_e; i--) 
#define FORIT(i, m) for (__typeof((m).begin()) i=(m).begin(); i!=(m).end(); ++i)
#define SET(t,v) memset((t), (v), sizeof(t))
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize( unique( ALL(c) ) - (c).begin() )

#define sz size()
#define pb push_back
#define VI vector<int>
#define VS vector<string>

typedef long long LL;
typedef long double LD;
typedef pair<int,int> pii;

#define D(x) if(1) cout << __LINE__ <<" "<< #x " = " << (x) << endl;
#define D2(x,y) if(1) cout << __LINE__ <<" "<< #x " = " << (x) \
     <<", " << #y " = " << (y) << endl;

int main() {
    int T;
    while(cin >> T && T) {
        int dx, dy;
        scanf("%d %d", &dx, &dy);

        REP(i, T) {
            int x, y, ax, ay;
            scanf("%d %d", &x, &y);

            ax = x - dx, ay = y - dy;

            if (!ax || !ay) printf("divisa");
            else if (ax < 0 && ay > 0) printf("NO");
            else if (ax > 0 && ay > 0) printf("NE");
            else if (ax > 0 && ay < 0) printf("SE");
            else if (ax < 0 && ay < 0) printf("SO");

            printf("\n");
        }
    }
	return 0;
}
