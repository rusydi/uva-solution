#include<algorithm>
#include<iostream>
#include<set>

using namespace std;

int main()
{
    set<string> S;
    string input;

    while(true)
    {
        cin >> input;
        if (input == "XXXXXX")
            break;
        S.insert(input);
    }

    while(true)
    {
        cin >> input;
        if (input == "XXXXXX")
            break;

        bool word_exist = false;
        sort(input.begin(), input.end());
        do
        {
            if (S.count(input))
            {
                cout << input << endl;
                word_exist = true;
            }
        } while ( next_permutation(input.begin(), input.end()) );
        if (!word_exist)
            cout << "NOT A VALID WORD" << endl;
        cout << "******" << endl;
    }
    return 0;
}
