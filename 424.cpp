#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

string add(string a, string b) {
    reverse(a.begin(), a.end());
    reverse(b.begin(), b.end());
    string maxnum;
    string ans;
    int carry=0;
    int minlen;

    if (a.length() > b.length()) maxnum = a;
    else maxnum = b;

    minlen = min(a.length(), b.length());

    for(int i=0; i<minlen; i++) {
        int result = carry + (a[i] - '0') + (b[i] - '0');

        if (result > 9) {
            result %= 10;
            carry = 1;
        }
        else
            carry = 0;
        
        ans.push_back( (char) result + '0' );
    }
    for(int i=minlen; i<maxnum.length(); i++) {
        int result = carry + (maxnum[i] - '0');

        if (result > 9) {
            result %= 10;
            carry = 1;
        }
        else
            carry = 0;
        ans.push_back( (char) result + '0' );
    }

    if (carry == 1) ans.push_back('1');
    
    reverse(ans.begin(), ans.end());
    return ans;
}

int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif

    string strnum;
    string result = "0";
    vector<string> listnum;

    while(cin >> strnum && strnum != "0")
        listnum.push_back(strnum);

    for(int i=0; i<listnum.size(); i++)
        result = add(result, listnum[i]);

    cout << result << endl;

    return 0;
}

