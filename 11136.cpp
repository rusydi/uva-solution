#include<iostream>
#include<set>

using namespace std;

int main()
{
    long long T;
    while(cin >> T && T)
    {
        multiset<long long> S;
        long long K, ans = 0;
        while(T--)
        {
            cin >> K;

            while(K--)
            {
                long long n;
                cin >> n;
                S.insert(n);
            }

            auto begin = S.begin();
            auto end = S.end(); --end;

            ans += (*end) - (*begin);

            S.erase(begin);
            S.erase(end);
        }
        cout << ans << endl;
    }

    return 0;
}
