#include<iostream>
#include<unordered_map>
#include<vector>

using namespace std;

int main()
{
    unsigned n, m;
    while(cin >> n >> m)
    {
        unordered_map<unsigned, vector<unsigned>> M;

        for(unsigned i = 0; i < n; ++i)
        {
            unsigned element;
            cin >> element;
            M[element].emplace_back(i + 1);
        }

        for(unsigned i = 0; i < m; ++i)
        {
            unsigned k, v;
            unsigned ans = 0;

            cin >> k >> v;
            --k;
            ans = (k < M[v].size()) ? M[v][k] : 0;
            cout << ans << endl;
        }
    }
    return 0;
}
