#include<algorithm>
#include<iostream>
#include<queue>
#include<set>

using namespace std;

int main()
{
    unsigned scenario = 0, nteams;
    while(cin >> nteams && nteams)
    {
        ++scenario;
        cout << "Scenario #" << scenario << endl;

        vector<set<unsigned>> teams(nteams);
        vector<queue<unsigned>> Q(nteams);
        queue<unsigned> TQ;
        vector<bool> in_queue(nteams, false);

        for(unsigned i = 0; i < nteams; ++i)
        {
            unsigned nmembers;
            cin >> nmembers;

            for(unsigned j = 0; j < nmembers; ++j)
            {
                unsigned member_id;
                cin >> member_id;
                teams[i].insert(member_id);
            }
        }

        string command;
        while(cin >> command && command != "STOP")
        {
            if (command == "ENQUEUE")
            {
                unsigned element;
                cin >> element;

                size_t team_id = find_if(teams.begin(), teams.end(),
                                    [&element](const set<unsigned> & S)
                                    { return S.count(element); }) - teams.begin();

                Q[team_id].push(element);

                if (TQ.empty() || !in_queue[team_id])
                {
                    in_queue[team_id] = true;
                    TQ.push(team_id);
                }
            }
            else if (command == "DEQUEUE")
            {
                unsigned front_team = TQ.front();

                cout << Q[ front_team ].front() << endl;
                Q[ front_team ].pop();

                if (Q[ front_team ].empty())
                {
                    in_queue[front_team] = false;
                    TQ.pop();
                }
            }
        }

        cout << endl;
    }
    return 0;
}
