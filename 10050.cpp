#include <iostream>
#include <numeric>
#include <vector>

using namespace std;

int main()
{
    unsigned T;
    cin >> T;

    while(T--)
    {
        unsigned N;
        cin >> N;

        std::vector<unsigned> days;
        days.resize(N + 1);

        unsigned P;
        cin >> P;

        while(P--)
        {
            unsigned h;
            cin >> h;

            for(unsigned i = h; i <= N; i += h)
            {
                days[i] = 1;
            }
        }

        for(unsigned i = 6; i <= N; i += 7)
        {
            days[i] = 0;
            days[i + 1] = 0;
        }

        unsigned ans = accumulate(days.begin(), days.end(), 0);

        std::cout << ans << std::endl;

    }
    return 0;
}
