#include<algorithm>
#include<iostream>
#include<map>
#include<string>
#include<vector>

using namespace std;

int minsz, maxsz;
unsigned int n;
vector<string> newspapers;
vector<vector<unsigned int> > answers;
map<string, int> M;
unsigned int answer;

void backtrack(unsigned int index)
{
    int weight = __builtin_popcount(answer);
    if (weight >= minsz && weight <= maxsz)
        answers[weight].push_back(answer);

    for(unsigned int i = index+1; i < n; ++i)
    {
        answer ^= (1 << i);
        backtrack(i);
        answer ^= (1 << i);
    }
}

void print_answers()
{
    for(int size = minsz; size <= maxsz; ++size)
    {
        cout << "Size " << size << endl;
        for(unsigned int i = 0; i < answers[size].size(); ++i)
        {
            unsigned int ntz = __builtin_ctz(answers[size][i]);
            cout << newspapers[ntz];
            for(unsigned int j = ntz+1; j < n; ++j)
            {
                if (answers[size][i] & (1 << j))
                    cout << ", " << newspapers[j];
            }
            cout << endl;
        }
        cout << endl;
    }
}

int main()
{
    int T; cin >> T;
    cin.ignore();
    for(int t = 0; t < T; ++t)
    {
        string fline, line;
        n = 0; answer = 0;
        newspapers.clear();
        answers.clear();

        if (t == 0) cin.ignore();
        if (t > 0) cout << endl;

        getline(cin, fline);
        while(getline(cin, line) && !line.empty())
        {
            newspapers.push_back(line);
            M[line] = n;
            ++n;
        }

        size_t space_i = fline.find(' ');
        if (space_i != string::npos)
        {
            minsz = stoi(fline.substr(0, space_i));
            maxsz = stoi(fline.substr(space_i+1, fline.size() - space_i - 1));
        }
        else if (fline[0] == '*')
        {
            minsz = 1;
            maxsz = n;
        }
        else
        {
            minsz = stoi(fline);
            maxsz = stoi(fline);
        }
        answers.resize(n+1);
        backtrack(-1);

        print_answers();

    }
    return 0;
}
