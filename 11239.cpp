#include<algorithm>
#include<iostream>
#include<map>
#include<set>
#include<string>
#include<vector>

using namespace std;

int main()
{
    string line;
    map<string, set<string>> projectdb;
    map<string, set<string>> studentdb;
    string projname;
    while(getline(cin, line) && line != "0")
    {
        if (all_of(line.begin(), line.end(), [](char c){ return isupper(c) || c == ' ';}))
        {
            projname = line;
            projectdb[line] = move(set<string>());
        }
        else if (all_of(line.begin(), line.end(), [](char c){ return islower(c) || isdigit(c); }))
        {
            if (line != "1")
            {
                projectdb[projname].insert(line);
                studentdb[line].insert(projname);
            }
            else
            {
                for(const auto & entry : studentdb)
                {
                    if (entry.second.size() > 1)
                    {
                        for_each(entry.second.begin(), entry.second.end(),
                            [&entry, &projectdb](const string & projname)
                            {
                                projectdb[projname].erase( projectdb[projname].find(entry.first) );
                            });
                    }
                }

                vector<pair<string, size_t>> ans;
                for(const auto & entry : projectdb)
                {
                    ans.emplace_back(entry.first, entry.second.size());
                }
                sort(ans.begin(), ans.end(), [](const pair<string, size_t> & p1, const pair<string, size_t> & p2)
                {
                    if (p1.second != p2.second)
                        return p1.second > p2.second;
                    else
                        return p1.first < p2.first;
                });

                for(const auto & p : ans)
                    cout << p.first << " " << p.second << endl;

                //clear data
                projname.clear();
                projectdb.clear();
                studentdb.clear();
                continue;
            }
        }
    }
    return 0;
}
