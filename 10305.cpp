#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <queue>
#include <stack>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

bool G[101][101];
int  indegree[101];

int main() {
    int M, N;

    while(cin >> N >> M && N) {
        queue<int> Q;
        vector<int> ans;

        memset(G, 0, sizeof(G));
        memset(indegree, 0, sizeof(indegree));

        while(M--) {
            int n1, n2;
            cin >> n1 >> n2;

            G[n1][n2] = true;
            indegree[n2]++;
        }

        //enqueue all nodes with 0 indegree
        for(int i=1; i<=N; i++) {
            if (indegree[i] == 0) {
                Q.push(i);
                ans.push_back(i);
            }
        }

        while(!Q.empty()) {
            int n = Q.front(); Q.pop();

            for(int i=1; i<=N; i++) {
                if (!G[n][i]) continue;
                indegree[i]--;
                if (indegree[i] == 0) {
                    Q.push(i);
                    ans.push_back(i);
                }
            }
        }

        for(int i=0; i<ans.size(); i++) {
            if (i > 0) cout << " ";
            cout << ans[i];
        }
        cout << endl;
    }
    return 0;
}

