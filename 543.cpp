#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

#define MAX 1000001

bool isnprime[MAX];
vector<int> prime;

void sieve() {
    isnprime[0] = isnprime[1] = true;

    for(int i=2; i<MAX; i++) {
        if (!isnprime[i]) {
            for(int j=i+i; j<MAX; j+=i)
                isnprime[j] = true;
            prime.push_back(i);
        }
    }
}

int main() {
    sieve();
    long int N;
    
    while(cin >> N && N) {
        int i=0;
        while(!binary_search(prime.begin(), prime.end(), N-prime[i])) i++;
        cout << N << " = " << prime[i] << " + " << N-prime[i] << endl;
    }

    return 0;
}

