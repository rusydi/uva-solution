#include<algorithm>
#include<iostream>
#include<queue>
#include<string>
#include<unordered_map>
#include<vector>

using namespace std;

vector<string> split(const string & str, const string & delim)
{
    vector<string> ret;

    size_t i = 0;
    while(true)
    {
        size_t j = str.find(delim, i);

        if (j == string::npos)
        {
            ret.emplace_back( str.substr(i, string::npos) );
            break;
        }
        else
        {
            ret.emplace_back( str.substr(i, j-i+1) );
            i = j + 3;
        }
    }

    return ret;
}

int main()
{
    size_t T;
    cin >> T;
    for(size_t t = 1; t <= T; ++t)
    {
        size_t P, N;
        cin >> P >> N;

        vector<vector<size_t>> G;
        queue<size_t> Q;

        cin.ignore();
        string line;

        unordered_map<string, size_t> authindex;
        size_t counter = 0;
        while(P--)
        {
            getline(cin, line);
            vector<string> authors = split(line.substr(0, line.find(":")), ".,");

            for_each(authors.begin(), authors.end(), [&counter, &authindex, &G](const string & author)
            {
                if (!authindex.count(author))
                {
                    authindex[author] = counter++;
                    G.emplace_back(vector<size_t>());
                }
            });

            for(size_t i = 0; i < authors.size()-1; ++i)
            {
                for(size_t j = i + 1; j < authors.size(); ++j)
                {
                    G[ authindex.at(authors[i]) ].push_back( authindex.at(authors[j]) );
                    G[ authindex.at(authors[j]) ].push_back( authindex.at(authors[i]) );
                }
            }
        }

        vector<char> visited(G.size(), 0);
        vector<signed> distance(G.size(), -1);

        size_t index = authindex["Erdos, P."];
        Q.push(index);
        visited[index] = 1;
        distance[index] = 0;

        while(!Q.empty())
        {
            index = Q.front();
            Q.pop();

            for(size_t i = 0; i < G[index].size(); ++i)
            {
                size_t neighbour = G[index][i];
                if (visited[ neighbour ])
                    continue;

                Q.push(neighbour);
                visited[neighbour] = 1;
                distance[neighbour] = distance[index] + 1;
            }
        }

        cout << "Scenario " << t << endl;
        string name;
        while(N--)
        {
            getline(cin, name);
            cout << name << " ";

            if (!authindex.count(name) || distance[ authindex.at(name) ] == -1)
                cout << "infinity" << endl;
            else
                cout << distance[authindex.at(name)] << endl;
        }
    }
    return 0;
}
