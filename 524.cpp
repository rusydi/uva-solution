#include<array>
#include<cmath>
#include<iostream>
#include<vector>

using namespace std;

#define UB 33

array<bool, UB> is_prime;
array<bool, 17> is_checked;
vector<int> numbers;
size_t n;

void init()
{
    is_prime.fill(true);

    for(size_t i = 2; i < sqrt(UB)+1; ++i)
        if (is_prime[i])
            for(size_t j = i*i; j < UB; j += i)
                is_prime[j] = false;
}

void backtrack(int num)
{
    if (numbers.size() == n)
    {
        if (is_prime[numbers.front() + numbers.back()])
        {
            cout << numbers[0];
            for(size_t i = 1; i < numbers.size(); ++i)
                cout << " " << numbers[i];
            cout << endl;
        }
        return;
    }

    for(size_t i = 1; i <= n; ++i)
    {
        if (!is_checked[i] && is_prime[numbers.back() + i])
        {
            numbers.push_back(i);
            is_checked[i] = true;
            backtrack(i);
            is_checked[i] = false;
            numbers.pop_back();
        }
    }
}

int main()
{
    init();
    int T=0;
    while(cin >> n)
    {
        numbers.clear();
        is_checked.fill(false);

        numbers.emplace_back(1);
        is_checked[1] = true;

        ++T;
        if (T > 1) cout << endl;
        cout << "Case " << T << ":" << endl;
        backtrack(2);
    }
    return 0;
}
