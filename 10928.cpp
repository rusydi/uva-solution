#include<algorithm>
#include<iostream>
#include<sstream>
#include<string>
#include<vector>

using namespace std;

int main()
{
    unsigned T;
    cin >> T;
    while(T--)
    {
        unsigned P;
        cin >> P;
        cin.ignore();

        vector<unsigned> n_neighbours(P);
        string line;
        for(unsigned i = 0; i < P; ++i)
        {
            stringstream ss;
            getline(cin, line);

            ss << line;
            unsigned adjnode, nadjnode = 0;
            while(ss >> adjnode)
                ++nadjnode;

            n_neighbours[i] = nadjnode;
        }
        unsigned min_neighbours = *(min_element(n_neighbours.begin(), n_neighbours.end()));
        unsigned ctr = 0;
        for(size_t i = 0; i < n_neighbours.size(); ++i)
        {
            if (n_neighbours[i] == min_neighbours)
            {
                if (ctr > 0)
                    cout << " ";
                cout << i + 1;
                ctr++;
            }
        }
        cout << endl;
    }
    return 0;
}
