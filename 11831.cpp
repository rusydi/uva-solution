#include<algorithm>
#include<array>
#include<iostream>

using namespace std;

int main()
{
    array<array<char, 100>, 100> A;

    const array<char, 4> orientation = {{'N', 'L', 'S', 'O'}};
    const array<signed char, 4> step_x = {{-1, 0, 1,  0}};
    const array<signed char, 4> step_y = {{ 0, 1, 0, -1}};

    signed nrows, ncols, S;
    signed x=0, y=0;
    signed char o=0;
    signed ans;

    while(cin >> nrows >> ncols >> S && nrows && ncols && S)
    {
        ans = 0;
        for(signed i = 0; i < nrows; ++i)
        {
            for(signed j = 0; j < ncols; ++j)
            {
                cin >> A[i][j];
                if (find(orientation.cbegin(), orientation.cend(), A[i][j]) !=
                    orientation.cend())
                {
                    x = i;
                    y = j;
                    switch(A[i][j])
                    {
                        case 'N': o = 0; break;
                        case 'L': o = 1; break;
                        case 'S': o = 2; break;
                        case 'O': o = 3; break;
                    }
                }
            }
        }

        for(signed s = 0; s < S; ++s)
        {
            char step;
            cin >> step;

            if (step == 'D') //90 deg to right
                o = (o + 1) % 4;
            else if (step == 'E') //90 deg to left
                o = (o - 1 + 4) % 4;
            else if (step == 'F') //move forward
            {
                signed tx = x + step_x[o];
                signed ty = y + step_y[o];

                if ((A[tx][ty] == '#') || (tx < 0) || (tx > nrows-1) ||
                        (ty < 0) || (ty > ncols-1))
                    continue;
                else if (A[tx][ty] == '*')
                {
                    ++ans;
                    A[tx][ty] = '.';
                }

                x = tx; y = ty;
            }
        }
        cout << ans << endl;
    }
    return 0;
}
