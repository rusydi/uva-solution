#include<iostream>
#include<string>
#include<vector>

using namespace std;

unsigned int n;
vector<unsigned int> result;
int k;

void binary_rep(unsigned int b)
{
    for(int i=n-1; i >= 0; --i)
    {
        if (b & (1 << i)) printf("1");
        else printf("0");
    }
    printf("\n");
}

void backtrack(unsigned int b, int index)
{
    for(int i=index-1; i >= 0; --i)
    {
        b ^= (1 << i);
        if (__builtin_popcount(b) <= k) backtrack(b, i);
        b ^= (1 << i);
    }

    if (__builtin_popcount(b) == k)
        result.push_back(b);
}

int main()
{
    int T; cin >> T;
    while(T--)
    {
        result.clear();
        cin >> n >> k;

        backtrack(0, n);

        for(auto it = result.rbegin(); it != result.rend(); ++it)
            binary_rep(*it);

        if (T) cout << endl;
    }
    return 0;
}
