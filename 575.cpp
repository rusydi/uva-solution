#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <string>
#include <cmath>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    string skewnum;

    while(cin >> skewnum && skewnum != "0") {
        long int decnum=0;
        double exponent = skewnum.length();

        for(int i=0; i<skewnum.length(); i++) {
            decnum += ((skewnum[i] - '0') * ((pow(2.0, exponent)) - 1));
            exponent--;
        }
        cout << decnum << endl;
    }
    return 0;
}

