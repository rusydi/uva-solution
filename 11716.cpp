#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cmath>

using namespace std;

int main() {
    int tcase;
    string line;

    cin >> tcase;
    cin.ignore();

    for(int p=0; p<tcase; p++) {
        getline(cin, line);

        if ( ( sqrt(line.length()) - (int)sqrt(line.length()) ) > 0) {
            cout << "INVALID" << endl;
        }
        else {
            int size = sqrt(line.length());

            for(int i=0; i<size; i++) {
                for(int j=i; j<line.length(); j+=size) {
                    cout << line[j];
                }
            }
            cout << endl;
        }
    }

    return 0;
}
