
#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <climits>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    int M[3][3];

    while( scanf("%d %d %d %d %d %d %d %d %d", &M[0][0], &M[0][1], &M[0][2], &M[1][0],
            &M[1][1], &M[1][2], &M[2][0], &M[2][1], &M[2][2]) == 9 ) {
        int sumB, sumG, sumC;
        int tempSum;
        int minSum = INT_MAX;
        string result;

        sumB = M[0][0] + M[1][0] + M[2][0];
        sumG = M[0][1] + M[1][1] + M[2][1];
        sumC = M[0][2] + M[1][2] + M[2][2];

        //BCG
        tempSum = sumB - M[0][0] + sumC - M[1][2] + sumG - M[2][1];
        if (tempSum < minSum) {
            minSum = tempSum;
            result = "BCG";
        }

        //BGC
        tempSum = sumB - M[0][0] + sumG - M[1][1] + sumC - M[2][2];
        if (tempSum < minSum) {
            minSum = tempSum;
            result = "BGC";
        }

        //CBG
        tempSum = sumC - M[0][2] + sumB - M[1][0] + sumG - M[2][1];
        if (tempSum < minSum) {
            minSum = tempSum;
            result = "CBG";
        }

        //CGB
        tempSum = sumC - M[0][2] + sumG - M[1][1] + sumB - M[2][0];
        if (tempSum < minSum) {
            minSum = tempSum;
            result = "CGB";
        }

        //GBC
        tempSum = sumG - M[0][1] + sumB - M[1][0] + sumC - M[2][2];
        if (tempSum < minSum) {
            minSum = tempSum;
            result = "GBC";
        }

        //GCB
        tempSum = sumG - M[0][1] + sumC - M[1][2] + sumB - M[2][0];
        if (tempSum < minSum) {
            minSum = tempSum;
            result = "GCB";
        }

        printf("%s %d\n", result.c_str(), minSum);
    }

    return 0;
}

