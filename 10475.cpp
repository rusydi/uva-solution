#include<algorithm>
#include<cstring>
#include<iostream>
#include<map>
#include<set>
#include<string>
#include<vector>

using namespace std;

int t, p;
size_t s;
vector<string> output, topics;
map<string, int> M;
bool prohibited[16][16];
bool visited[65536];

bool is_prev_output()
{
    unsigned int mask = 0;
    for(size_t i = 0; i < output.size(); ++i)
        mask |= (1 << M[output[i]]);
    return visited[mask];
}

bool is_prohibited(const string & topic)
{
    int index = M[topic];
    for(size_t i = 0; i < output.size(); ++i)
        if (prohibited[index][M[output[i]]])
            return true;
    return false;
}

void backtrack(int index)
{
    if (output.size() == s)
    {
        if (!is_prev_output())
        {
            unsigned int mask = 0;
            cout << output[0];
            mask |= (1 << M[output[0]]);
            for(size_t i=1; i < s; ++i)
            {
                cout << " " << output[i];
                mask |= (1 << M[output[i]]);
            }
            cout << endl;
            visited[mask] = true;
        }
        return;
    }

    for(int i = index+1; i < t; ++i)
    {
        if (output.empty() || !is_prohibited(topics[i]))
        {
            output.push_back(topics[i]);
            backtrack(i);
            output.pop_back();
        }
    }
}

int main()
{
    int T;
    cin >> T;
    for(int __i=1; __i <= T; ++__i)
    {
        output.clear();
        topics.clear();
        M.clear();
        memset(visited, 0, sizeof(bool)*65536);
        memset(prohibited, 0, sizeof(bool)*16*16);

        cin >> t >> p >> s;

        for(int i=0; i < t; ++i)
        {
            string str;
            cin >> str;
            transform(str.begin(), str.end(), str.begin(), [](char c){return toupper(c);});
            topics.emplace_back(str);
        }
        sort(topics.begin(), topics.end(), [](const string & a, const string & b) {
            if (a.size() != b.size()) return a.size() > b.size();
            return a < b;
        });
        for(size_t i = 0; i < topics.size(); ++i)
            M[topics[i]] = i;

        for(int i=0; i < p; ++i)
        {
            string first, second;
            cin >> first >> second;

            transform(first.begin(), first.end(), first.begin(), [](char c){return toupper(c);});
            transform(second.begin(), second.end(), second.begin(), [](char c){return toupper(c);});

            prohibited[M[first]][M[second]] = true;
            prohibited[M[second]][M[first]] = true;
        }

        cout << "Set " << __i << ":" << endl;
        backtrack(-1);
        cout << endl;
    }
    return 0;
}
