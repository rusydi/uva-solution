#include<cstdio>
#include<iostream>
#include<vector>

using namespace std;

bool ans;
int n, m;
pair<int, int> f_piece, l_piece;
vector< pair<int, int> > pieces, seq;
vector<bool> is_used;

void search(int counter)
{
    if ((counter == n) && (seq.back().second == l_piece.first))
        ans = true;

    if (ans || counter == n) return;

    pair<int, int> p(seq.back());

    for(int i = 0; i < m; ++i)
    {
        if (!is_used[i])
        {
            if ((p.second == pieces[i].first) || (p.second == pieces[i].second))
            {
                is_used[i] = true;
                if (p.second == pieces[i].first)
                    seq.push_back(pieces[i]);
                else
                    seq.push_back(make_pair(pieces[i].second, pieces[i].first));
                search(counter+1);
                seq.pop_back();
                is_used[i] = false;
            }
        }
    }
}

int main()
{
    while(cin >> n && n)
    {
        ans = false;
        is_used.clear();
        pieces.clear();
        seq.clear();

        cin >> m;

        is_used.resize(m);
        cin >> f_piece.first >> f_piece.second;
        cin >> l_piece.first >> l_piece.second;

        for(int i = 0; i < m; ++i)
        {
            pair<int, int> p;
            cin >> p.first >> p.second;
            pieces.push_back(p);
        }

        seq.push_back(f_piece);
        search(0);

        if (ans) cout << "YES" << endl;
        else cout << "NO" << endl;
    }
    return 0;
}
