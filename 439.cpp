#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <queue>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int dist[10][10];

int cx[] = {1,  1, 2,  2, -1, -1, -2, -2};
int cy[] = {2, -2, 1, -1,  2, -2,  1, -1};

bool inrange(int x, int y) {
    return (x >= 0 && x < 8 && y >= 0 && y < 8);
}

int BFS(int x1, int y1, int x2, int y2) {
    queue<pair<int, int> > Q;

    dist[x1][y1] = 0;
    Q.push(make_pair(x1, y1));

    while(!Q.empty()) {
        pair<int, int> t = Q.front(); Q.pop();

        if (t.first == x2 && t.second == y2)
            return dist[t.first][t.second];

        for(int i=0; i<8; i++) {
            int tx = t.first + cx[i];
            int ty = t.second + cy[i];
            
            if ( inrange(tx, ty) && dist[tx][ty] == -1 ) {
                dist[tx][ty] = dist[t.first][t.second] + 1;
                Q.push(make_pair(tx, ty));
            }
        }
    }
}

int main() {
    string str1, str2;

    while(cin >> str1 >> str2) {
        int x1, y1, x2, y2;

        memset(dist, -1, sizeof(dist));

        x1 = str1[0] - 'a'; y1 = str1[1] - '1';
        x2 = str2[0] - 'a'; y2 = str2[1] - '1';

        cout << "To get from " << str1 << " to " << str2 << " takes " <<
                BFS(x1, y1, x2, y2) << " knight moves." << endl;
    }

    return 0;
}
