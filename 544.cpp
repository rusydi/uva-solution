#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<cassert>
#include<climits>
using namespace std;

#define REP(i,n) for(int i=0, _e(n); i<_e; i++)
#define FOR(i,a,b) for(int i(a), _e(b); i<=_e; i++)
#define FORD(i,a,b) for(int i(a), _e(b); i>=_e; i--) 
#define FORIT(i, m) for (__typeof((m).begin()) i=(m).begin(); i!=(m).end(); ++i)
#define SET(t,v) memset((t), (v), sizeof(t))
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize( unique( ALL(c) ) - (c).begin() )

#define sz size()
#define pb push_back
#define VI vector<int>
#define VS vector<string>

typedef long long LL;
typedef long double LD;
typedef pair<int,int> pii;

#define D(x) if(1) cout << __LINE__ <<" "<< #x " = " << (x) << endl;
#define D2(x,y) if(1) cout << __LINE__ <<" "<< #x " = " << (x) \
     <<", " << #y " = " << (y) << endl;

struct Node {
	int at, cost;
	inline bool operator<(const Node &nextNode) const {
		if (cost != nextNode.cost) return cost > nextNode.cost;
		if (at != nextNode.at) return at > nextNode.at;
		return false;
	}
};

int main() {
	int N, R, t=1;
	while(cin >> N >> R && N && R) {
		map<string, int> city2index;
		priority_queue<Node> PQ;
		int w[201][201] = {{}};
		bool visited[201] = {};
		int path[201] = {};
		int max_edge[201] = {};
		int city_index = 1;

		REP(i, R) {
			string city1, city2;
			int distance;
			cin >> city1 >> city2 >> distance;

			if (!city2index[city1]) city2index[city1] = city_index++;
			if (!city2index[city2]) city2index[city2] = city_index++;
			w[city2index[city1]-1][city2index[city2]-1] = w[city2index[city2]-1][city2index[city1]-1] = -1*distance;
		}
		
		string src, dst;
		cin >> src >> dst;

		Node start; start.at = city2index[src]-1; start.cost = 0;
		PQ.push(start);

		while(!PQ.empty()) {
			Node node = PQ.top(); PQ.pop();

			if (visited[node.at]) {
				continue;
			}
			visited[node.at] = true;

			REP(i, N) {
				if (w[node.at][i] != 0 && !visited[i] && (w[node.at][i] < max_edge[i])) {
					path[i] = node.at;
					max_edge[i] = w[node.at][i];
					Node nextNode; nextNode.at = i; nextNode.cost = w[node.at][i];
					PQ.push(nextNode);
				}
			}

			if (node.at == city2index[dst]-1) break;
		}

		int currpath = path[city2index[dst]-1];
		int capacity = -1*w[city2index[dst]-1][currpath];
		
		while(currpath != city2index[src]-1) {
			if ( -1*w[currpath][path[currpath]] < capacity )
				capacity = -1*w[currpath][path[currpath]];
			currpath = path[currpath];
		}

		printf("Scenario #%d\n", t++);
		printf("%d tons\n\n", capacity);
	}
	return 0;
}
