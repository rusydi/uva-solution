#include<cstdio>
#include<cstring>
#include<iostream>
#include<string>
#include<vector>

using namespace std;

vector<string> path;
char G[8][8];
int l, w;
int st_x, st_y, fin_x, fin_y;
const char valid_letter[] = {'I', 'E', 'H', 'O', 'V', 'A', '#'};
bool is_found;

bool is_valid(char c)
{
    for(size_t i = 0; i < 7; ++i)
        if (c == valid_letter[i])
            return true;
    return false;
}

void findpath(int x, int y)
{
    if (G[x][y] == '#')
    {
        for(size_t i = 0; i < path.size(); ++i)
        {
            if (i > 0) printf(" ");
            printf("%s", path[i].c_str());
        }
        printf("\n");

        is_found = true;
    }

    if (is_found) return;

    if ((x >= 0 && x <= l-1) && (y >= 0 && y <= w-1))
    {
        if (x + 1 <= l-1)
        {
            if (is_valid(G[x+1][y]))
            {
                path.push_back(string("forth"));
                findpath(x+1, y);
                path.pop_back();
            }
        }

        if (y + 1 <= w-1)
        {
            if (is_valid(G[x][y+1]))
            {
                if (path.empty() || path.back() != "left")
                {
                    path.push_back(string("right"));
                    findpath(x, y+1);
                    path.pop_back();
                }
            }
        }

        if (y - 1 >= 0)
        {
            if (is_valid(G[x][y-1]))
            {
                if (path.empty() || path.back() != "right")
                {
                    path.push_back(string("left"));
                    findpath(x, y-1);
                    path.pop_back();
                }
            }
        }
    }
}

int main()
{
    int T; cin >> T;
    while(T--)
    {
        is_found = false;
        path.clear();
        memset(G, 0, sizeof(char)*8*8);

        cin >> l >> w;
        for(int i = l-1; i >= 0; --i)
            for(int j = 0; j < w; ++j)
                cin >> G[i][j];

        st_x = 0; fin_x = l-1;
        for(int j=0; j < w; ++j)
            if (G[0][j] == '@')
                st_y = j;
        for(int j=0; j < w; ++j)
            if (G[l-1][j] == '#')
                fin_y = j;

        findpath(st_x, st_y);
    }
    return 0;
}
