#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    int T; cin >> T;
    for(int t=0; t<T; t++) {
        int ans=0;
        int n1, n2;
        int a, b;
        
        cin >> n1 >> n2;

        a = min(n1, n2);
        b = max(n1, n2);

        if (a % 2 == 0) a++;
        if (b % 2 == 0) b--;

        for(int i=a; i<=b; i+=2)
            ans += i;
        
        cout << "Case " << t+1 << ": " << ans << endl;
    }

    return 0;
}

