#include<cstdio>
#include<iostream>
#include<numeric>
#include<vector>

using namespace std;

vector<int> solution;
vector<int> durations;
vector<int> path;
int maxsum;
int N;
int ntracks;

void backtrack(int track_id, int curr_sum)
{
    if (curr_sum > maxsum) {
        maxsum = curr_sum;
        solution = path;
    }

    for(int next_track = track_id + 1; next_track < ntracks; ++next_track)
    {
        if (durations[next_track] + curr_sum <= N)
        {
            path.push_back(durations[next_track]);
            backtrack(next_track, durations[next_track] + curr_sum);
            path.pop_back();
        }
    }
}

int main()
{
    while(cin >> N >> ntracks)
    {
        maxsum = 0;
        path.clear();
        solution.clear();
        durations.clear();
        durations.reserve(ntracks);

        for(int i=0; i < ntracks; ++i)
            cin >> durations[i];

        backtrack(-1, 0);

        for(auto v : solution)
            cout << v << " ";

        cout << "sum:" << maxsum << endl;
    }
    return 0;
}
