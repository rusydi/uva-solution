#include<iostream>
#include<queue>

using namespace std;

int main()
{
    unsigned N;
    while(cin >> N && N)
    {
        priority_queue<unsigned long long, vector<unsigned long long>,
                        greater<unsigned long long>> Q;
        unsigned long long n, cost=0;

        for(unsigned i = 0; i < N; cin >> n, Q.push(n), ++i);

        for(unsigned i = 0; i < N-1; ++i)
        {
            unsigned long long sum = 0;
            sum += Q.top(); Q.pop();
            sum += Q.top(); Q.pop();
            cost += sum;
            Q.push(sum);
        }

        cout << cost << endl;
    }
    return 0;
}
