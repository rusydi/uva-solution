#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

typedef unsigned long long ULL;

ULL cycle(ULL n) {
    ULL CYC=1;

    while(n != 1) {
        if (n % 2 == 0)
            n /= 2;
        else
            n = 3*n + 1;
        CYC++;
    }
    return CYC;
}

int main() {
    ULL num1, num2;

    while(cin >> num1 >> num2) {
        ULL A, Z, MAXCYC;

        A = min(num1, num2);
        Z = max(num1, num2);

        MAXCYC = cycle(A);

        for(int i=A+1; i<=Z; i++) {
            ULL temp = cycle(i);
            if (temp > MAXCYC)
                MAXCYC = temp;
        }

        cout << num1 << " " << num2 << " " << MAXCYC << endl;
    }
}
