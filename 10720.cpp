#include<algorithm>
#include<iostream>
#include<vector>

using namespace std;

int main()
{
    unsigned n;
    while(cin >> n && n)
    {
        unsigned degsum = 0;
        vector<unsigned> degrees(n);
        bool flag = true;

        for(unsigned i = 0; i < n; ++i)
        {
            cin >> degrees[i];
            degsum += degrees[i];
        }
        sort(degrees.begin(), degrees.end(), greater<unsigned>());
        unsigned lsum = 0;
        for(unsigned k = 1; k <= n; ++k)
        {
            lsum += degrees[k-1];
            unsigned rsum = 0;
            for(unsigned i = k + 1; i <= n; ++i)
                rsum += min(degrees[i-1], k);

            if (lsum > k*(k-1) + rsum)
            {
                flag = false;
                break;
            }
        }

        if ((degsum % 2 == 0) && (flag))
            cout << "Possible" << endl;
        else
            cout << "Not possible" << endl;
    }
    return 0;
}
