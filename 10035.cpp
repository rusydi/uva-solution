#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <sstream>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    string num1, num2;
    unsigned long long n1, n2;

    while(cin >> n1 >> n2 && (n1 || n2) ) {
        stringstream ss;
        ss << n1; ss >> num1; ss.clear();
        ss << n2; ss >> num2; ss.clear();

        reverse(num1.begin(), num1.end());
        reverse(num2.begin(), num2.end());
        
        int minlength = min(num1.length(), num2.length());
        int carry=0;
        int ans=0;
        string longnum;

        if (num1.length() > num2.length())
            longnum = num1;
        else
            longnum = num2;

        for(int i=0; i<minlength; i++) {
            int result;

            result = carry + (num1[i] - '0') + (num2[i] - '0');
            if (result > 9) {
                carry = 1;
                ans++;
            }
            else carry=0;
        }

        for(int i=minlength; i<longnum.length(); i++) {
            int result;

            result = carry + (longnum[i] - '0');
            if (result > 9) {
                carry = 1;
                ans++;
            }
            else carry=0;
        }

        if (ans == 0)
            cout << "No carry operation." << endl;
        else if (ans == 1)
            cout << "1 carry operation." << endl;
        else
            cout << ans << " carry operations." << endl;
    }

    return 0;
}

