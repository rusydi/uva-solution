/*
 ID: rusydi.1
 PROG: test
 LANG: C++
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <queue>

using namespace std;

#ifndef ONLINE_JUDGE
#define D(x)    cerr << #x << "=" << x << endl;
#define DA(x,i,j)\
    {\
        cerr << #x << "=";\
        for (int wholawhowhatwhere=i; wholawhowhatwhere<j; ++wholawhowhatwhere)\
            cout << " " << wholawhowhatwhere << ": " << x[wholawhowhatwhere];\
        cout << endl;\
    };
#else
#define D(x)
#define DA(x,i,j)
#endif

typedef unsigned long long ULL;
typedef long long LL;

bool G[101][101];
int indegree[101];

int main() {
    int N;

    while(cin >> N && N) {
        int srcnode, destnode;
        queue<int> Q;
        vector<int> ans;

        memset(G, 0, sizeof(G));
        memset(indegree, 0, sizeof(indegree));
        
        for(int i=0; i<N; i++) {
            int n_node;
            cin >> n_node;
            indegree[i] = n_node;
            
            for(int j=0; j<n_node; j++) {
                int srcnode;
                cin >> srcnode;
                G[srcnode-1][i] = true;
            }
        }

        for(int i=N-1; i>=0; i--) {
            if (indegree[i] == 0) {
                Q.push(i);
                ans.push_back(i);
            }
        }

        while(!Q.empty()) {
            int node = Q.front(); Q.pop();

            for(int i=N-1; i>=0; i--) {
                if (!G[node][i]) continue;

                indegree[i]--;
                if (indegree[i] == 0) {
                    Q.push(i);
                    ans.push_back(i);
                }
            }
        }
        cout << ans[ans.size()-1]+1 << endl;
    }

    return 0;
}

