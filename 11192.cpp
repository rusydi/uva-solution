#include<algorithm>
#include<iostream>

using namespace std;

int main()
{
    unsigned groupsize;
    while(cin >> groupsize && groupsize)
    {
        string str;
        cin >> str;

        unsigned blocksize = str.size() / groupsize;

        auto first = str.begin();
        auto last = str.begin() + blocksize;

        while(true)
        {
            reverse(first, last);

            if (last == str.end())
                break;

            first = last;
            last = last + blocksize;
        }
        cout << str << endl;
    }
    return 0;
}
