#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    string s, t;
    while(cin >> s >> t) {
        int p=0;
        int ctr=0;

        for(int i=0; i<t.length(); i++) {
            if (t[i] == s[p]) {
                p++;
                ctr++;
            }
        }
        if (ctr == s.length())
            cout << "Yes" << endl;
        else
            cout << "No" << endl;
    }

    return 0;
}

