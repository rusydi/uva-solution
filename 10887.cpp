#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <map>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    int T; cin >> T;

    for(int t=0; t<T; t++) {
        int M, N;
        vector<string> list1, list2;
        map<string, bool> MAP;

        cin >> M >> N;
        cin.ignore();

        for(int i=0; i<M; i++) {
            string temp;
            getline(cin, temp);
            list1.push_back(temp);
        }

        for(int i=0; i<N; i++) {
            string temp;
            getline(cin, temp);
            list2.push_back(temp);
        }

        list1.erase(unique(list1.begin(), list1.end()), list1.end());
        list2.erase(unique(list2.begin(), list2.end()), list2.end());

        for(int i=0; i<list1.size(); i++) {
            for(int j=0; j<list2.size(); j++)
                MAP[list1[i]+list2[j]] = true;
        }

        cout << "Case " << t+1 << ": " << MAP.size() << endl;
    }

    return 0;
}

