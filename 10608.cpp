#include <algorithm>
#include <iostream>
#include <vector>

template<typename T>
class union_find
{
public:
    typedef typename std::vector<T>::iterator iterator;
    typedef typename std::vector<T>::const_iterator const_iterator;

    union_find() {}
    union_find(T size) : _v(size) { init(size); }

    inline void init(T size)
    {
        T c = 0;
        std::generate(_v.begin(), _v.end(), [&c](){ return c++; });
    }

    inline T find_set(T elem)
    {
        return (_v[elem] == elem) ? elem : (_v[elem] = find_set(_v[elem]));
    }

    inline bool is_same_set(T l, T r) { return find_set(l) == find_set(r); }
    inline void union_set(T l, T r) { _v[find_set(l)] = find_set(r); }

    inline iterator begin() { return _v.begin(); }
    inline iterator end() { return _v.end(); }

    inline const_iterator begin() const { return _v.cbegin(); }
    inline const_iterator end() const { return _v.cend(); }

    inline bool empty() const { return _v.empty(); }
private:
    std::vector<T> _v;
};

int main()
{
    int Tcases;
    std::cin >> Tcases;

    while(Tcases--)
    {
        std::size_t N, M;
        std::cin >> N >> M;

        union_find<std::size_t> U(N);
        std::vector<std::size_t> maxelem(N, 0);

        for(std::size_t i = 0; i < M; ++i)
        {
            std::size_t A, B;

            std::cin >> A >> B;
            --A; --B;

            U.union_set(A, B);
        }

        for(std::size_t i = 0; i < N; ++i)
        {
            std::size_t parent = U.find_set(i);
            ++maxelem[parent];
        }

        std::cout << *std::max_element(maxelem.begin(), maxelem.end()) << std::endl;
    }

    return 0;
}
