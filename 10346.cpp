#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    int N, K;

    while(cin >> N >> K) {
        int cigar=N;
        int temp=N;
        int remain=0;

         do {
            while(temp > 0) {
                remain += temp % K;
                temp /= K;
                cigar += temp;
            }
            temp = remain;
            remain=0;
        } while(temp >= K);

        cout << cigar << endl;
    }

    return 0;
}

