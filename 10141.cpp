#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    ULL numreq, numprop;
    string garb;
    string propname;
    ULL counter=0;

    double price, req;

    string curr_prop;
    ULL curr_req;
    double curr_price;

    while (cin >> numreq >> numprop && numreq && numprop) {

        for(int i=0; i<numreq; i++) {
            cin.ignore();
            getline(cin, garb);
        }

        for(int i=0; i<numprop; i++) {
            getline(cin, propname);
            cin >> price >> req;

            if (i == 0) {
                curr_prop = propname;
                curr_req = req;
                curr_price = price;
            }

            if (req > curr_req) {
                curr_prop = propname;
                curr_req = req;
                curr_price = price;
            }
            else if (req == curr_req) {
                if ((price - curr_price) < 0) {
                    curr_prop = propname;
                    curr_req = req;
                    curr_price = price;
                }
            }

            for(int j=0; j<req; j++) {
                cin.ignore();
                getline(cin, garb);
            }
        }
        if (counter > 0)
            cout << endl;
        cout << "RFP #" << counter+1 <<endl;
        cout << curr_prop << endl;
        counter++;
    }
    return 0;
}
