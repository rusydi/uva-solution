#include<iostream>
#include<string>

using namespace std;

bool isempty;

void backtrack(const string & s)
{
    if (isempty || s.size() == 1) return;
    if (s.empty()) { isempty = true; return; }

    for(size_t i = 0; i < s.size()-1; )
    {
        if (s[i] != s[i+1]) {++i; continue;}
        size_t counter = 0;
        string s_new = s;
        for(size_t j = i; s[j] == s[i]; ++j) ++counter;
        s_new.erase(i, counter);
        backtrack(s_new);
        i += counter-1;
    }
}

int main()
{
    int T; cin >> T;
    while(T--)
    {
        string str;
        isempty = false;
        cin >> str;
        backtrack(str);
        cout << isempty << endl;
    }
    return 0;
}
