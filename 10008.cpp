#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

typedef struct {
    char c_index;
    int  freq;
} DATA;

int main ()
{
    DATA stat_list[26];
    int n;

    for (int i=0; i<26; i++) {
        stat_list[i].c_index = i+65;
        stat_list[i].freq = 0;
    }

    scanf("%d", &n);
    cin.ignore();
    for (int i=0; i<n; i++) {
        string line;

        getline(cin, line);

        for(int j=0; j<line.length(); j++) {
            if (isalpha(line[j])) {
                stat_list[toupper(line[j]) - 65].freq++;
            }
        }
    }

    for (int i=0; i<26-1; i++) {
        for (int j=i+1; j<26; j++) {
            if (stat_list[i].freq < stat_list[j].freq) {
                DATA temp;

                temp.c_index = stat_list[i].c_index;
                temp.freq    = stat_list[i].freq;

                stat_list[i].c_index = stat_list[j].c_index;
                stat_list[i].freq = stat_list[j].freq;

                stat_list[j].c_index = temp.c_index;
                stat_list[j].freq    = temp.freq;

            }
            else if (stat_list[i].freq == stat_list[j].freq) {
                if (stat_list[i].c_index > stat_list[j].c_index) {
                    DATA temp;

                    temp.c_index = stat_list[i].c_index;
                    temp.freq    = stat_list[i].freq;

                    stat_list[i].c_index = stat_list[j].c_index;
                    stat_list[i].freq = stat_list[j].freq;

                    stat_list[j].c_index = temp.c_index;
                    stat_list[j].freq    = temp.freq;
                }
            }
        }
    }

    for (int i=0; i<26; i++) {
        if (stat_list[i].freq != 0) {
            cout<<stat_list[i].c_index<<" "<<stat_list[i].freq<<endl;
        }
    }

    return 0;
}


