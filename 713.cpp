#include <iostream>
#include <string>
#include <cstring>
#include <cmath>
#include <vector>
#include <stack>
#include <queue>
#include <map>
#include <cstdlib>
#include <cstdio>

using namespace std;

int main(int argc, char** argv) {

    int testcase;
    string num1, num2;
    
    cin >> testcase;

    for (int q=0; q<testcase; q++) {
        int carry = 0;
        string result;

        cin >> num1 >> num2;

        int minlen = min(num1.length(), num2.length());
        int maxlen = max(num1.length(), num2.length());

        string maxstr;

        if (num1.length() > num2.length())
            maxstr = num1;
        else
            maxstr = num2;

        for (int i=0; i < minlen; i++) {
            int temp = carry + (num1[i] - '0') + (num2[i] - '0');

            if (temp > 9) {
                result[i] = (temp % 10) + '0';
                carry = 1;
            }
            else {
                result[i] = temp + '0';
                carry = 0;
            }
        }

        for (int i=minlen; i<maxlen; i++) {
            int temp = carry + (maxstr[i] - '0');

            if (temp > 9) {
                result[i] = (temp % 10) + '0';
                carry = 1;
            }
            else {
                result[i] = temp + '0';
                carry = 0;
            }
        }

        if (carry == 1) {
            result[maxlen] = carry + '0';
            result[maxlen+1] = '\0';
        }
        else
            result[maxlen] = '\0';
            
        int index = 0;
        while (result[index] == '0')
            index++;

        for (; index < strlen(result.c_str()); index++)
            cout << result[index];
        cout << endl;
    }

    return 0;
}
