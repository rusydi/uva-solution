#include <iostream>
#include <cstring>
#include <cstdlib>
#include <stdlib.h>

using namespace std;

int main() {

    char number[15];

    while(1) {
        int  N_result;

        memset(&number, 0, sizeof(number));

        scanf("%s", number);

        if (strncmp(number, "0", 1) == 0)
            break;

        while(strlen(number) != 1) {
            N_result = 0;
            
            for (int i=0; i<strlen(number); i++) {
                N_result += (number[i] - '0');
            }                

            sprintf(number, "%d", N_result);
        }

        printf("%s\n", number);
    }

    return 0;
}