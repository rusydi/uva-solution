#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

ULL array_counter[1000010];

int main() {
    string onezero;
    ULL counter = 0;

    while(cin >> onezero) {
        ULL numquery;
        char firstchar;
        ULL c=0;

        memset(&array_counter, 0, sizeof(array_counter));
        firstchar = onezero[0];

        for(int i=1; i<onezero.length(); i++) {
            if (onezero[i] != firstchar) {
                firstchar = onezero[i];
                c++;
                array_counter[i] = c;
            }
            else
                array_counter[i] = c;
        }

        cout << "Case " << counter+1 << ":" << endl;
        cin >> numquery;
        for(int p=0; p<numquery; p++) {
            ULL num1, num2;
            
            cin >> num1 >> num2;

            if ( array_counter[num1] == array_counter[num2] )
                cout << "Yes" << endl;
            else
                cout << "No" << endl;
        }
        counter++;
    }
    return 0;
}
