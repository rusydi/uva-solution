#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

bool isnum(char c) {
    if ( (c-'0' >=0) && (c-'0'<=9) )
        return true;
    else
        return false;
}

int main() {
    string str;

    while(getline(cin, str)) {
        int counter=0;
        
        for(int i=0; i<str.length(); i++) {
            
            if (isnum(str[i])) {
                counter += str[i] - '0';
            }
            else if (str[i] == '!')
                cout << endl;
            else {
                for(int j=0; j<counter; j++) {
                    if (str[i] == 'b')
                        cout << " ";
                    else
                        cout << str[i];
                }
                counter=0;
            }
        }
        cout << endl;
    }

    return 0;
}
