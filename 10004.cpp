#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<cassert>
#include<climits>
using namespace std;

#define REP(i,n) for(int i=0, _e(n); i<_e; i++)
#define FOR(i,a,b) for(int i(a), _e(b); i<=_e; i++)
#define FORD(i,a,b) for(int i(a), _e(b); i>=_e; i--) 
#define FORIT(i, m) for (__typeof((m).begin()) i=(m).begin(); i!=(m).end(); ++i)
#define SET(t,v) memset((t), (v), sizeof(t))
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize( unique( ALL(c) ) - (c).begin() )

#define sz size()
#define pb push_back
#define VI vector<int>
#define VS vector<string>

typedef long long LL;
typedef long double LD;
typedef pair<int,int> pii;

#define D(x) if(1) cout << __LINE__ <<" "<< #x " = " << (x) << endl;
#define D2(x,y) if(1) cout << __LINE__ <<" "<< #x " = " << (x) \
     <<", " << #y " = " << (y) << endl;

int main() {
	int N;

	while(scanf("%d", &N) == 1 && N) {
		vector< vector<int> > G(N);
		queue<int> Q;
		bool visited[200] = {};
		bool colors[200] = {};
		bool colorable = true;	
		int num_edges;

		scanf("%d", &num_edges);
		REP(i, num_edges) {
			int n1, n2;
			scanf("%d %d", &n1, &n2);
			G[n1].pb(n2); G[n2].pb(n1);
		}

		Q.push(0);

		while(!Q.empty() && colorable) {
			int node = Q.front(); Q.pop();

			if (visited[node]) continue;
			visited[node] = true;

			REP(i, G[node].size()) {
				if ( (visited[G[node][i]]) && !(colors[G[node][i]] ^ colors[node]) ) {
					colorable = false;
					break;
				}
				else if (!visited[G[node][i]]){
					colors[G[node][i]] = !colors[node];
					Q.push(G[node][i]);
				}
			}
		}
		colorable ? printf("BICOLORABLE.\n") : printf("NOT BICOLORABLE.\n");
	}
	return 0;
}
