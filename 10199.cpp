#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <map>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

bool G[101][101];
bool visited[101];
int ctrnode=0;
int N;

void DFS(int n) {
    if (visited[n])
        return;

    visited[n] = true;
    ctrnode++;

    for(int i=0; i<N; i++) {
        if (G[n][i]) DFS(i);
    }
}

int main() {
   
    int t=0;
    while(cin >> N && N) {
        map<string, int> M;
        vector<string> ans; ans.clear();
        memset(G, 0, sizeof(G));
        ctrnode=0;

        for(int i=0; i<N; i++) {
            string cityname;
            cin >> cityname;
            M[cityname] = i;
        }

        int E; cin >> E;
        for(int i=0; i<E; i++) {
            string city1, city2;
            cin >> city1 >> city2;

            G[M[city1]][M[city2]] = G[M[city2]][M[city1]] = 1;
        }

        for(map<string, int>::iterator it=M.begin(); it!=M.end(); it++) {
            int reachable=0;
            int adjnode;
            ctrnode=0;

            memset(visited, 0, sizeof(visited));
            DFS(it->second);
            reachable=ctrnode;

            memset(visited, 0, sizeof(visited));
            visited[it->second] = true;
            ctrnode=0;

            for(int i=0; i<N; i++) {
                if (G[it->second][i]) {
                    adjnode = i;
                    break;
                }
            }

            DFS(adjnode);

            if ( (reachable > 1) && ctrnode != reachable-1)
                ans.push_back(it->first);
        }

        if (t > 0) cout << endl;
        cout << "City map #" << t+1 << ": " << ans.size() << " camera(s) found" << endl;
        for(int i=0; i<ans.size(); i++)
            cout << ans[i] << endl;
        t++;
    }

    return 0;
}

