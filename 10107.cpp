#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    long long N;
    vector<long long> V;
    
    while(cin >> N) {

        V.push_back(N);
        sort(V.begin(), V.end());

        int size = V.size();

        if (size % 2 == 0)
            cout << (V[size/2] + V[size/2 - 1]) / 2 << endl;
        else
            cout << V[(size/2)] << endl;
    }

    return 0;
}

