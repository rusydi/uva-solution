#include<algorithm>
#include<iostream>
#include<sstream>
#include<vector>

using namespace std;

vector<string> to_title(const string & s)
{
    vector<string> ret;

    istringstream iss(s);
    for(string temp; iss >> temp; )
    {
        transform(temp.begin(), temp.end(), temp.begin(), ::tolower);
        ret.emplace_back(std::move(temp));
    }

    return ret;
}

int main()
{
    vector<vector<string>>  titles;
    vector<string> ignored_words;

    for(string input; cin >> input && input != "::";
        ignored_words.emplace_back(std::move(input)));

    sort(ignored_words.begin(), ignored_words.end());

    cin.ignore();
    for(string input; getline(cin, input);
        titles.emplace_back(std::move(to_title(input))));

    vector<pair<size_t, size_t>> db;
    for(size_t i = 0; i < titles.size(); ++i)
        for(size_t j = 0; j < titles[i].size(); ++j)
            if (!binary_search(ignored_words.begin(), ignored_words.end(), titles[i][j]))
                db.emplace_back(i, j);

    sort(db.begin(), db.end(),
        [&titles](const pair<size_t, size_t> & p1, const pair<size_t, size_t> & p2)
        {
            const string & title_1 = titles[p1.first][p1.second];
            const string & title_2 = titles[p2.first][p2.second];
            bool ret;

            if (title_1 != title_2)
                ret = title_1 < title_2;
            else
            {
                if (p1.first != p2.first)
                    ret = p1.first < p2.first;
                else
                    ret = p1.second < p2.second;
            }
            return ret;
        });

    for(const auto & entry : db)
    {
        for(size_t i = 0; i < titles[entry.first].size(); ++i)
        {
            if (i > 0) cout << " ";
            if (i == entry.second)
            {
                for(const auto & c : titles[entry.first][entry.second])
                    cout << (char) toupper(c);
            }
            else
                cout << titles[entry.first][i];
        }
        cout << endl;
    }

    return 0;
}
