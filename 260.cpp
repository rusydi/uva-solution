#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

char G[201][201];
bool visited[201][201];
int N;

void DFS(int x, int y) {
    if (x < 0 || y < 0 || x >= N || y >= N || visited[x][y] || G[x][y] == 'b')
        return;

    visited[x][y] = true;

    DFS(x-1, y-1); DFS(x-1, y); DFS(x, y-1);
    DFS(x, y+1); DFS(x+1, y); DFS(x+1, y+1);
}

int main() {
    int ctr=1;

    while(cin >> N && N) {
        memset(G, 0, sizeof(G));

        for(int i=0; i<N; i++) {
            for(int j=0; j<N; j++)
                cin >> G[i][j];
        }

        bool stat=false;

        for(int i=0; i<N; i++) {
            memset(visited, 0, sizeof(visited));

            if (G[i][0] == 'w')
                DFS(i, 0);

            for(int j=0; j<N; j++) {
                if (visited[j][N-1]) {
                    stat = true;
                    break;
                }
            }
            if (stat) break;
        }
        cout << ctr++ << " ";
        if (stat) cout << 'W';
        else cout << 'B';
        cout << endl;
    }

    return 0;
}

