/*
 ID: rusydi.1
 PROG: test
 LANG: C++
 */

#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <queue>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int src, dst;
int visited[10000];

int mod(int a, int b) {
    if (a >= 0)
        return a % b;
    else
        return b+a;
}

int merge(int n[]) {
    return (n[0]*1000 + n[1]*100 + n[2] * 10 + n[3]);
}

vector<int> getadj(int n) {
    vector<int> ans;
    int num[4];
    
    num[0] = n/1000; n %= 1000;
    num[1] = n/100 ; n %= 100;
    num[2] = n/10  ; n %= 10;
    num[3] = n;

    for(int i=0; i<4; i++) {
        int temp1 = mod(num[i] + 1, 10);
        int temp2 = mod(num[i] - 1, 10);
        int _temp[] = {num[0], num[1], num[2], num[3]};

        _temp[i] = temp1;
        ans.push_back(merge(_temp));

        _temp[i] = temp2;
        ans.push_back(merge(_temp));
    }

    return ans;
}

int BFS() {
    queue<int> Q;

    visited[src] = 0;
    Q.push(src);

    while(!Q.empty()) {
        int n = Q.front(); Q.pop();

        if (n == dst)
            return visited[dst];
        
        vector<int> adj = getadj(n);

        for(int i=0; i<adj.size(); i++) {
            if (visited[adj[i]] != -1) continue;

            visited[adj[i]] = visited[n] + 1;
            Q.push(adj[i]);
        }
    }
    return -1;
}

int main() {
    int T; cin >> T;

    while(T--) {
        int _src[4];
        int _dst[4];
        int nforbid;

        memset(visited, -1, sizeof(visited));

        cin >> _src[0] >> _src[1] >> _src[2] >> _src[3];
        cin >> _dst[0] >> _dst[1] >> _dst[2] >> _dst[3];

        src = merge(_src);
        dst = merge(_dst);

        cin >> nforbid;
        while(nforbid--) {
            int frbd, _frbd[4];
            cin >> _frbd[0] >> _frbd[1] >> _frbd[2] >> _frbd[3];

            frbd = merge(_frbd)
            visited[frbd] = 0;
        }

        cout << BFS() << endl;
    }

    return 0;
}

