#include<cmath>
#include<cstring>
#include<iomanip>
#include<iostream>
#include<vector>

using namespace std;

int row[9];
vector< vector<int> > valid_pos;

bool is_valid(int tryrow, int col)
{
    for(int prevcol = 1; prevcol < col; ++prevcol)
        if (row[prevcol] == tryrow || abs(row[prevcol] - tryrow) == abs(prevcol - col))
            return false;
    return true;
}

void backtrack(int col)
{
    for(int tryrow=1; tryrow <= 8; ++tryrow)
    {
        if (is_valid(tryrow, col))
        {
            row[col] = tryrow;
            if (col == 8)
                valid_pos.emplace_back(vector<int>(row+1, row+9));
            else
                backtrack(col + 1);
        }
    }
}

int main()
{
    backtrack(1);

    int T = 0;
    while(cin >> row[1] >> row[2] >> row[3] >> row[4] >> row[5] >> row[6] >> row[7] >> row[8])
    {
        int minmoves = 7;

        for(auto pos : valid_pos)
        {
            int nmoves = 0;
            for(int i=0; i < 8; ++i)
                if (row[i+1] != pos[i])
                    nmoves++;
            minmoves = (nmoves < minmoves) ? nmoves : minmoves;
        }
        printf("Case %d: %d\n", ++T, minmoves);
    }

    return 0;
}
