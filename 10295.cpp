#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <sstream>

using namespace std;

map<string, int> M;

int main() {
    M.clear();
    int Nword, Njob;

    cin >> Nword >> Njob;

    for(int i=0; i<Nword; i++) {
        string job;
        int salary;

        cin >> job >> salary;

        M[job] = salary;
    }

    for(int i=0; i<Njob; i++) {

        unsigned long long payment = 0;
        string line;

        while(getline(cin, line) && line != ".") {
            stringstream ss;
            string jobkw;

            ss.clear();
            ss << line;

            while(ss >> jobkw) {
                if (M[jobkw])
                    payment += M[jobkw];
            }
        }

        cout << payment << endl;
    }

    return 0;
}
