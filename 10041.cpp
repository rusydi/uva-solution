#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<cassert>
#include<climits>
using namespace std;

#define REP(i,n) for(int i=0, _e(n); i<_e; i++)
#define FOR(i,a,b) for(int i(a), _e(b); i<=_e; i++)
#define FORD(i,a,b) for(int i(a), _e(b); i>=_e; i--) 
#define FORIT(i, m) for (__typeof((m).begin()) i=(m).begin(); i!=(m).end(); ++i)
#define SET(t,v) memset((t), (v), sizeof(t))
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize( unique( ALL(c) ) - (c).begin() )

#define sz size()
#define pb push_back
#define VI vector<int>
#define VS vector<string>

typedef long long LL;
typedef long double LD;
typedef pair<int,int> pii;

#define D(x) if(1) cout << __LINE__ <<" "<< #x " = " << (x) << endl;
#define D2(x,y) if(1) cout << __LINE__ <<" "<< #x " = " << (x) \
     <<", " << #y " = " << (y) << endl;

int main() {
	int T; scanf("%d", &T);
	while(T--) {
		int R; scanf("%d", &R);
		vector<int> v;

		REP(i, R) {
			int t; scanf("%d", &t);
			v.pb(t);
		}

		sort(v.begin(), v.end());
		int sum=0;
		if ( (R % 2 != 0) || (v[R/2] == v[R/2+1]) ) {
			REP(i, R) {
				if (i == R/2) continue;
				sum += abs(v[R/2] - v[i]);
			}
		}
		else {
			int sum1=0, sum2=0;
			REP(i, R) {
				if (i != R/2) sum1 += abs(v[R/2] - v[i]);
				if (i != R/2 + 1) sum2 += abs(v[R/2 + 1] - v[i]);
			}
			sum1 < sum2 ? sum = sum1 : sum = sum2;
		}
		printf("%d\n", sum);
	}
	return 0;
}
