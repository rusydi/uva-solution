#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    unsigned T;
    cin >> T;

    while(T--)
    {
        unsigned P;
        cin >> P;

        vector<unsigned> seat(P, 0);
        vector<unsigned> pindex(P, 0);

        unsigned half = 0;
        for(unsigned i = 0; i < P; ++i)
        {
            cin >> seat[i];
            half += seat[i];
        }
        half >>= 1;

        for(unsigned i = 1; i < (1 << P); ++i)
        {
            unsigned sum = 0, ctr = 0;
            vector<unsigned> members;

            for(unsigned mask = i; mask != 0; mask >>= 1, ++ctr)
            {
                if (mask & 1)
                {
                    sum += seat[ctr];
                    members.emplace_back(ctr);
                }
            }

            if (sum <= half)
                continue;

            for(const auto & member : members)
            {
                if (sum - seat[member] <= half)
                    ++pindex[member];
            }
        }

        for(unsigned i = 0; i < P; ++i)
            cout << "party " << i + 1 << " has power index " << pindex[i] << endl;
        cout << endl;
    }
    return 0;
}
