#include<iostream>
#include<string>

using namespace std;

int main()
{
    string word;
    while(cin >> word)
    {
        char newchar = 0;
        for(const auto & c : word)
        {
            char cans = 0;
            if (c == 'B' || c == 'F' || c == 'P' || c == 'V')
                cans = '1';
            else if (c == 'C' || c == 'G' || c == 'J' || c == 'K' || c == 'Q' ||
                        c == 'S' || c == 'X' || c == 'Z')
                cans = '2';
            else if (c == 'D' || c == 'T')
                cans = '3';
            else if (c == 'L')
                cans = '4';
            else if (c == 'M' || c == 'N')
                cans = '5';
            else if (c == 'R')
                cans = '6';

            if (cans == newchar)
                continue;
            else
            {
                if (cans != 0)
                    cout << cans;
                newchar = cans;
            }
        }
        cout << endl;
    }
    return 0;
}
