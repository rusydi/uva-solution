#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	int N;

	while( (scanf("%d", &N) == 1) && N) {
		vector<int> age;
	
		for(int i=0; i<N; i++) {
			int temp;
			scanf("%d", &temp);
			age.push_back(temp);
		}
		sort(age.begin(), age.end());
		for(int i=0; i<N; i++) {
			if (i > 0) printf(" ");
			printf("%d", age[i]);
		}
		printf("\n");
	}
	return 0;
}
