#include<iostream>
#include<string>

using namespace std;

int main()
{
    string line;
    size_t c = 0;
    while(cin >> line && line != "*")
    {
        cout << "Case " << ++c << ": ";
        if (line == "Hajj")
            cout << "Hajj-e-Akbar" << endl;
        else if (line == "Umrah")
            cout << "Hajj-e-Asghar" << endl;
    }
    return 0;
}
