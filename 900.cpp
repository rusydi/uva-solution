#include <iostream>

using namespace std;

unsigned long long fibonacci_num[55];

void init() {
    fibonacci_num[0] = 1;
    fibonacci_num[1] = 2;

    for (int i=2; i<=50; i++)
        fibonacci_num[i] = fibonacci_num[i-1] + fibonacci_num[i-2];
}

int main() {

    init();
    int number;

    while (cin >> number && number) {
        cout << fibonacci_num[number-1] << endl;
    }
    return 0;
}