#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

bool comp(string s1, string s2) {
    if (s1[0] != s2[0]) {
        if (s1[0] == 'H') s1[0] = 'Z';
        if (s2[0] == 'H') s2[0] = 'Z';

        return s1[0] < s2[0];
    }
    else {
        int t1 = 0, t2 = 0;

        t1 = s1[1] - '0';
        t2 = s2[1] - '0';

        switch(s1[1]) {
            case 'T' : t1 = 10; break;
            case 'J' : t1 = 11; break;
            case 'Q' : t1 = 12; break;
            case 'K' : t1 = 13; break;
            case 'A' : t1 = 14; break;
        }

        switch(s2[1]) {
            case 'T' : t2 = 10; break;
            case 'J' : t2 = 11; break;
            case 'Q' : t2 = 12; break;
            case 'K' : t2 = 13; break;
            case 'A' : t2 = 14; break;
        }

        return t1 < t2;
    }
}

int main() {
    char C;
    string row1, row2;

    //South = 0, West = 1, North = 2, East = 3
    //const int players[] = {0, 1, 2, 3};
    const char player_name[] = {'S', 'W', 'N', 'E'};

    while(cin >> C && C != '#') {
        size_t startplayer = 0;

        cin >> row1;
        cin >> row2;

        vector< vector<string> > player_deck;
        player_deck.resize(4);

        switch(C) {
            case 'S' : startplayer = 1; break;
            case 'W' : startplayer = 2; break;
            case 'N' : startplayer = 3; break;
            case 'E' : startplayer = 0; break;
        }

        size_t turn = startplayer;

        for (size_t i=0; i < row1.length(); i += 2) {
            player_deck[ turn ].emplace_back(row1.substr(i, 2));
            turn = (turn + 1) % 4;
        }

        for (size_t i=0; i < row2.length(); i += 2) {
            player_deck[ turn ].emplace_back(row2.substr(i, 2));
            turn = (turn + 1) % 4;
        }

        for (size_t i=0; i < 4; i++)
            sort(player_deck[i].begin(), player_deck[i].end(), comp);

        for (size_t i=0; i < 4; i++) {
            cout << player_name[i] << ": ";
            for (size_t j=0; j < player_deck[i].size(); j++) {
                if (j) cout << " ";
                cout << player_deck[i][j];
            }
            cout << endl;
        }
    }

    return 0;
}
