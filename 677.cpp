#include<cassert>
#include<cstring>
#include<iostream>
#include<vector>

using namespace std;

bool G[10][10];
bool visited[10];
bool path_found;
int n_nodes, n_walks;
vector<int> path;

void search_path(int node, int counter)
{
    if (counter == n_walks)
    {
        printf("(");
        printf("%d", path[0]+1);
        for(size_t i = 1; i < path.size(); ++i)
            printf(",%d", path[i]+1);
        printf(")\n");

        path_found = true;
        return;
    }

    for(int i = 0; i < n_nodes; ++i)
    {
        if (G[node][i] && !visited[i])
        {
            path.push_back(i);
            visited[i] = true;
            search_path(i, counter+1);
            visited[i] = false;
            path.pop_back();
        }
    }
}

int main()
{

    while(cin >> n_nodes)
    {
        if (n_nodes == -9999)
        {
            cout << endl;
            continue;
        }

        cin >> n_walks;

        path_found = false;
        memset(G, 0, sizeof(bool)*10*10);
        path.clear();
        memset(visited, 0, sizeof(bool)*10);

        for(int i = 0; i < n_nodes; ++i)
            for(int j = 0; j < n_nodes; ++j)
                cin >> G[i][j];

        path.push_back(0);
        visited[0] = true;
        search_path(0, 0);

        if (!path_found)
            cout << "no walk of length " << n_walks << endl;
    }
    return 0;
}
