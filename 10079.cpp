#include<cstdio>
int main() {
	long long N;
	while(scanf("%lli", &N) == 1 && N >= 0) {
		if (N % 2 == 0) printf("%lli\n", (N/2 * (N+1)) + 1);
		else printf("%lli\n", (N * ((N+1)/2)) + 1);
	}
	return 0;
}
