#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

char G[101][101];
bool visited[101][101];
int R, C;
int ctr=0;

void DFS(int x, int y) {
    if (x < 0 || y < 0 || x >= R || y >= C || visited[x][y] || (G[x][y] == '*') ) {
        return;
    }
    visited[x][y] = true;
   
    DFS(x+1, y); DFS(x-1, y); DFS(x, y+1); DFS(x, y-1);
    DFS(x+1, y+1); DFS(x+1, y-1); DFS(x-1, y+1); DFS(x-1, y-1);
}

int main() {
    while(cin >> R >> C && R && C) {
        memset(G, 0, sizeof(G));
        memset(visited, 0, sizeof(visited));
        ctr=0;

        for(int i=0; i<R; i++) {
            for(int j=0; j<C; j++)
                cin >> G[i][j];
        }

        for(int i=0; i<R; i++) {
            for(int j=0; j<C; j++) {
                if (G[i][j] == '@' && !visited[i][j]) {
                    DFS(i, j);
                    ctr++;
                }
            }
        }
        cout << ctr << endl;
    }

    return 0;
}

