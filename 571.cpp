#include<iostream>
#include<map>
#include<queue>
#include<set>
#include<string>

using namespace std;

int main()
{
    int N;
    pair<int, int> C;
    while(cin >> C.first >> C.second >> N)
    {
        map< pair<int, int>, pair<int, int> > M;
        map< pair<int, int>, string> MS;
        queue< pair<int, int> > Q;
        set< pair<int, int> > visited;
        vector<string> answer;

        Q.push(make_pair(0, 0));
        M[make_pair(0, 0)] = make_pair(-1, -1);

        while(!Q.empty())
        {
            pair<int, int> p, next_p;

            p = Q.front();
            Q.pop();

            visited.insert(p);

            if (p.second == N)
            {
                pair<int, int> node = p;
                //while(M[node] != make_pair(-1, -1))
                while(node != make_pair(0, 0))
                {
                    answer.push_back(MS[node]);
                    node = M[node];
                }
                break;
            }

            /* fill A */
            if (p.first != C.first)
            {
                next_p = make_pair(C.first, p.second);
                if (visited.count(next_p) == 0)
                {
                    Q.push(next_p);
                    M[next_p] = p;
                    MS[next_p] = "fill A";
                }
            }

            /* fill B */
            if (p.second != C.second)
            {
                next_p = make_pair(p.first, C.second);
                if (visited.count(next_p) == 0)
                {
                    Q.push(next_p);
                    M[next_p] = p;
                    MS[next_p] = "fill B";
                }
            }

            /* empty A */
            if (p.first > 0)
            {
                next_p = make_pair(0, p.second);
                if (visited.count(next_p) == 0)
                {
                    Q.push(next_p);
                    M[next_p] = p;
                    MS[next_p] = "empty A";
                }
            }

            /* empty B */
            if (p.second > 0)
            {
                next_p = make_pair(p.first, 0);
                if (visited.count(next_p) == 0)
                {
                    Q.push(next_p);
                    M[next_p] = p;
                    MS[next_p] = "empty B";
                }
            }

            /* pour A B */
            if (p.first != 0 || p.second != C.second)
            {
                if (p.first <= C.second - p.second)
                    next_p = make_pair(0, p.second + p.first);
                else
                    next_p = make_pair(p.first - (C.second - p.second), C.second);
                if (visited.count(next_p) == 0)
                {
                    Q.push(next_p);
                    M[next_p] = p;
                    MS[next_p] = "pour A B";
                }
            }

            /* pour B A */
            if (p.second != 0 || p.first != C.first)
            {
                if (p.second <= C.first - p.first)
                    next_p = make_pair(p.first + p.second, 0);
                else
                    next_p = make_pair(C.first, p.second - (C.first - p.first));
                if (visited.count(next_p) == 0)
                {
                    Q.push(next_p);
                    M[next_p] = p;
                    MS[next_p] = "pour B A";
                }
            }
        }
        for(auto it = answer.rbegin(); it != answer.rend(); ++it)
            cout << *it << endl;
        cout << "success" << endl;
    }
    return 0;
}
