#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <sstream>
#include <string>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

LL mypow(LL a, LL b) {

    LL res = 1;

    if (b == 0)
        return 1;

    for(int i=0; i<b; i++) {
        res *= a;
    }

    return res;
}

int main() {
    string line1, line2;

    while(getline(cin, line1) && getline(cin, line2)) {
        stringstream ss1(line1);
        stringstream ss2(line2);

        int n;

        vector<LL> C; C.clear();
        vector<LL> X; X.clear();

        while(ss1 >> n)
            C.push_back(n);

        while(ss2 >> n)
            X.push_back(n);

        for(int i=0; i<X.size(); i++) {
            LL res=0;
            LL p = C.size()-1;
            
            for(int j=0; j<C.size(); j++) {
                res += C[j]*mypow(X[i], p--);
            }
            if (i != 0)
                cout << ' ';
            cout << res;
        }
        cout << endl;
    }

    return 0;
}

