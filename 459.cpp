#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<cassert>
#include<climits>
using namespace std;

#define REP(i,n) for(int i=0, _e(n); i<_e; i++)
#define FOR(i,a,b) for(int i(a), _e(b); i<=_e; i++)
#define FORD(i,a,b) for(int i(a), _e(b); i>=_e; i--) 
#define FORIT(i, m) for (__typeof((m).begin()) i=(m).begin(); i!=(m).end(); ++i)
#define SET(t,v) memset((t), (v), sizeof(t))
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize( unique( ALL(c) ) - (c).begin() )

#define sz size()
#define pb push_back
#define VI vector<int>
#define VS vector<string>

typedef long long LL;
typedef long double LD;
typedef pair<int,int> pii;

#define D(x) if(1) cout << __LINE__ <<" "<< #x " = " << (x) << endl;
#define D2(x,y) if(1) cout << __LINE__ <<" "<< #x " = " << (x) \
     <<", " << #y " = " << (y) << endl;

int pset[30];
int numDisjointSet;

void initSet(int n) {
    REP(i, n) pset[i] = i;
    numDisjointSet = n;
}

int findSet(int i) {
    return (pset[i] == i ? i : pset[i] = findSet(pset[i]));
    cout << findSet(0) << endl;
}

bool isSameSet(int i, int j) {
    return findSet(i) == findSet(j);
}

void printSet() {
    REP(i, 26) cout << pset[i] << " ";
    cout << endl;
}

void unionSet(int i, int j) {

    if (!isSameSet(i, j)) {
        //printSet();
        numDisjointSet--;
    }
    pset[findSet(i)] = findSet(j);

}


int main() {
    int T; cin >> T;
    while(T--) {
        char maxnode; cin >> maxnode;
        initSet(maxnode - 'A' + 1);

        cin.ignore();

        string conn_node;
        while(getline(cin, conn_node) && conn_node.size() > 0) {
            //cout << conn_node << endl;
            unionSet(conn_node[0] - 'A',  conn_node[1] - 'A');
        }
        cout << numDisjointSet << endl;

        if (T) cout << endl;

    }
	return 0;
}
