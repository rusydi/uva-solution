#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    int T; cin >> T;

    for(int t=0; t<T; t++) {
        int N, K, P;
        cin >> N >> K >> P;

        cout << "Case " << t+1 << ": " << (((K-1) + P) % N) + 1 << endl;
    }

    return 0;
}

