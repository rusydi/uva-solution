#include <iostream>
#include <vector>
#include <cstring>

using namespace std;

int R, C;
char grid[110][110];
int  result[110][110];

bool valid(int x, int y) {
    if (x < 0 || x >=R || y < 0 || y>=C || grid[x][y] == '*')
        return false;
    else
        return true;
}
int main(int argc, char** argv) {
    
    int counter=0;
    while (cin >> R >> C && R && C) {

        vector<int> X;
        vector<int> Y;

        X.clear();
        Y.clear();
        memset(&grid, 0, sizeof(grid));
        memset(&result, 0, sizeof(result));


        for(int i=0; i<R; i++) {
            for(int j=0; j<C; j++) {
                cin >> grid[i][j];
                if (grid[i][j] == '*') {
                    X.push_back(i);
                    Y.push_back(j);
                }
            }
        }

        if (X.size() > 0 || Y.size() > 0) {
            for(int i=0; i<X.size(); i++) {
                if (valid(X[i] - 1, Y[i])) {
                    result[X[i] - 1][Y[i]]++;
                }
                if (valid(X[i] + 1, Y[i])) {
                    result[X[i] + 1][Y[i]]++;
                }
                if (valid(X[i], Y[i] + 1)) {
                    result[X[i]][Y[i] + 1]++;
                }
                if (valid(X[i], Y[i] - 1)) {
                    result[X[i]][Y[i] - 1]++;
                }
                if (valid(X[i] - 1, Y[i] - 1)) {
                    result[X[i] - 1][Y[i] - 1]++;
                }
                if (valid(X[i] - 1, Y[i] + 1)) {
                    result[X[i] - 1][Y[i] + 1]++;
                }
                if (valid(X[i] + 1, Y[i] - 1)) {
                    result[X[i] + 1][Y[i] - 1]++;
                }
                if (valid(X[i] + 1, Y[i] + 1)) {
                    result[X[i] + 1][Y[i] + 1]++;
                }
            }
        }

        counter++;
        if (counter > 1)
            cout << endl;

        cout << "Field #"<<counter<<":"<<endl;
        int k=0;
        for(int i=0; i<R; i++) {
            for (int j=0; j<C; j++) {

                if (X.size() > 0 || Y.size() > 0) {
                    if ( (i == X[k]) && (j == Y[k]) ) {
                        cout << '*';
                        k++;
                    }
                    else
                        cout << result[i][j];
                }
                else
                    cout << result[i][j];
            }
            cout << endl;
        }
        
    }
    return 0;
}

