#include<algorithm>
#include<iostream>
#include<functional>
#include<set>
#include<vector>

using namespace std;

int t, n, sum;
bool found_sols;
vector<int> list_num;
vector<int> answer;
set< vector<int> > S;

void backtrack(int index)
{
    if (sum >= t)
    {
        if (sum == t && !S.count(answer))
        {
            found_sols = true;
            cout << answer[0];
            for(size_t i = 1; i < answer.size(); ++i)
                cout << "+" << answer[i];
            cout << endl;
            S.insert(answer);
        }
        return;
    }
    else
    {
        for(int i = index+1; i < n; ++i)
        {
            sum += list_num[i];
            answer.push_back(list_num[i]);
            backtrack(i);
            answer.pop_back();
            sum -= list_num[i];
        }
    }
}

int main()
{
    while(cin >> t >> n && t && n)
    {
        found_sols = false;
        list_num.clear();
        answer.clear();
        S.clear();
        sum = 0;

        list_num.resize(n);
        for(int i = 0; i < n; ++i)
            cin >> list_num[i];

        sort(list_num.begin(),list_num.end(), greater<int>());

        cout << "Sums of " << t << ":" << endl;
        backtrack(-1);
        if (!found_sols) cout << "NONE" << endl;
    }
    return 0;
}
