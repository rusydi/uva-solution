#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {

    int uglynum[1500]={};
    uglynum[0] = 1;
    int p,q,r;

    p=q=r=0;
    int i=1;

    while(uglynum[1499] == 0) {
        int P, Q, R;
        P = uglynum[p]*2;
        Q = uglynum[q]*3;
        R = uglynum[r]*5;

        if (P < Q && P < R) {
            uglynum[i] = P;
            i++;
            p++;
        }
        else if (Q < P && Q < R) {
            uglynum[i] = Q;
            i++;
            q++;
        }
        else if (R < P && R < Q) {
            uglynum[i] = R;
            i++;
            r++;
        }
        else if (P==Q || P==R)
            p++;
        else if (Q == R)
            q++;
    }

    printf("The 1500'th ugly number is %d.\n",uglynum[1499]);

    return 0;
}

