#include<iostream>
#include<cstring>
using namespace std;

#define MAX_SIZE 7500

long long ans[7500];

void init() {
	memset(ans, 0, sizeof(ans));
	int coin[] = {1, 5, 10, 25, 50};

	ans[0] = 1;
	for(int i=0; i<5; i++) {
		ans[coin[i]]++;
		for(int j=1; j<MAX_SIZE; j++) {
			if (ans[j] >= 1) {
				if (j + coin[i] <= MAX_SIZE)
					ans[j + coin[i]] += ans[j];
			}
		}
	}
}

int main() {
	int N;

	init();

	while(cin >> N) {
		cout << ans[N] << endl;
	}
}
