#include <iostream>
#include <vector>
#include <map>
#include <cmath>
#include <climits>

using namespace std;

typedef unsigned long long ULL;

map<ULL, ULL> fib_list;

void init() {
    ULL fib_array[100];

    memset(&fib_array, 0, sizeof(fib_array));

    fib_array[1] = 1;
    fib_array[2] = 2;

    int index=3;

    while(fib_array[index-1] + fib_array[index-2] <= 2147483647) {
        fib_array[index] = fib_array[index-1] + fib_array[index - 2];
        index++;
    }

    for (int i=1; i<index; i++)
        fib_list[fib_array[i]] = i;
}

int main() {
    int testcase;
    vector<ULL> list_num;
    init();

    cin >> testcase;

    for (int q=0; q<testcase; q++) {
        int num_char;
        string cipher;
        string plain(110, ' ');
        string temp;

        cipher.clear();
        temp.clear();
        list_num.clear();

        cin >> num_char;
        for (int i=0; i<num_char; i++) {
            ULL n;
            cin >> n;
            list_num.push_back(n);
        }

        cin.ignore();
        getline(cin, cipher);

        int j=0;

        for (int i=j=0; i<cipher.length(); i++) {
            if (isupper(cipher[i])) {
                temp[j] = cipher[i];
                j++;
            }
        }

        for (int i=0; i<list_num.size(); i++)
            plain[fib_list[list_num[i]] - 1] = toupper(temp[i]);

        //clean trailing space
        int end =  plain.length() - 1;
        while (plain[end] == ' ') {
            plain.erase(end, 1);
            end--;
        }

        cout << plain.c_str() << endl;
        plain.clear();
    }

    return 0;
}