#include<cmath>
#include<cstring>
#include<iomanip>
#include<iostream>
#include<vector>

using namespace std;

int row[8];
vector< vector<int> > valid_pos;

bool is_valid(int tryrow, int col)
{
    for(int prevcol = 0; prevcol < col; ++prevcol)
        if (row[prevcol] == tryrow || abs(row[prevcol] - tryrow) == abs(prevcol - col))
            return false;
    return true;
}

void backtrack(int col)
{
    for(int tryrow=0; tryrow < 8; ++tryrow)
    {
        if (is_valid(tryrow, col))
        {
            row[col] = tryrow;
            if (col == 7)
                valid_pos.emplace_back(vector<int>(row, row+8));
            else
                backtrack(col + 1);
        }
    }
}

int main()
{
    backtrack(0);

    int T; cin >> T;
    while(T--)
    {
        int scores[8][8] = {{}};
        int maxscore = 0;

        for(int i=0; i < 8; ++i)
            for(int j=0; j < 8; ++j)
            {
                int num;
                cin >> num;
                scores[i][j] = num;
            }

        for(auto pos : valid_pos)
        {
            int sum=0;
            for(int i=0; i < 8; ++i)
                sum += scores[i][pos[i]];
            maxscore = (sum > maxscore) ? sum : maxscore;
        }
        cout << setfill(' ') << setw(5) << maxscore << endl;
    }

    return 0;
}
