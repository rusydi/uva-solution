#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    const string norm = "ertyuiop[]dfghjkl;'cvbnm,.";
    const string repl = "qwertyuiopasdfghjklzxcvbnm";
    string line;

    while(getline(cin, line)) {
        for(int i=0; i<line.length(); i++) {
            if (line[i] != ' ')
                cout << repl [ norm.find_first_of(tolower(line[i])) ];
            else
                cout << " ";
        }
        cout << endl;
    }

    return 0;
}

