#include<iostream>
#include<map>

using namespace std;

int main()
{
    signed T;
    cin >> T;
    while(T--)
    {
        size_t ncand, nvote;
        string name, party;

        map<string, pair<signed, string>> candidates;

        scanf("%lu\n", &ncand);
        for(size_t i = 0; i < ncand; ++i)
        {
            getline(cin, name);
            getline(cin, party);
            candidates[name] = make_pair(0, party);
        }

        scanf("%lu\n", &nvote);
        for(size_t i = 0; i < nvote; ++i)
        {
            getline(cin, name);
            if (!candidates.count(name))
                continue;
            auto &candidate = candidates.at(name);
            candidate.first++;
        }

        signed maxval = -1;
        signed counter = 0;
        string winparty;
        for(const auto & entry : candidates)
        {
            if (entry.second.first > maxval)
            {
                maxval = entry.second.first;
                winparty = entry.second.second;
                counter = 1;
            }
            else if (entry.second.first == maxval)
                counter++;
        }

        if (counter > 1)
            cout << "tie" << endl;
        else
            cout << winparty << endl;

        if (T) cout << endl;
    }
    return 0;
}
