#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <string>

using namespace std;

map<char, char> M;

void mapping(string P, string K) {
    for(int i=0; i<P.length(); i++) {
        M[P[i]] = K[i];
    }
}

int main() {
    int testcase;
    cin >> testcase;

    for(int p=0; p<testcase; p++) {
        string str;
        string P, K;

        M.clear(); P.clear(); K.clear();

        if (p == 0) {
            cin.ignore();
            cin.ignore();
        }

        getline(cin, P);
        getline(cin, K);

        mapping(P, K);

        cout << K << endl << P;

        int z=0;

        while(getline(cin, str)) {

            if (str.length() == 0)
                break;

            cout << endl;
                        
            for(int i=0; i<str.length(); i++) {
                if (M[str[i]] != '\0')
                    cout << M[str[i]];
                else
                    cout << str[i];

            }
        }
        if (p != (testcase - 1) )
            cout << endl << endl ;
        else
            cout << endl;
    }

    return 0;
}
