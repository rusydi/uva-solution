#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

char G[30][30];
bool visited[30][30];
int N;

void DFS(int i, int j) {
    if (i < 0 || j < 0 || i >= N || j >= N || visited[i][j] || G[i][j] == '0')
        return;
    
    visited[i][j] = true;

    DFS(i, j+1); DFS(i, j-1); DFS(i+1, j); DFS(i-1, j);
    DFS(i+1, j+1); DFS(i+1, j-1); DFS(i-1, j+1); DFS(i-1, j-1);
}

int main() {
    int t=0;

    while(cin >> N) {
        memset(G, 0, sizeof(G));
        memset(visited, 0, sizeof(visited));

        int c=0;

        for(int i=0; i<N; i++) {
            for(int j=0; j<N; j++)
                cin >> G[i][j];
        }

        for(int i=0; i<N; i++) {
            for(int j=0; j<N; j++) {
                if (G[i][j] == '1' && !visited[i][j]) {
                    DFS(i, j);
                    c++;
                }
            }
        }

        cout << "Image number " << ++t << " contains " << c << " war eagles." << endl;
    }

    return 0;
}

