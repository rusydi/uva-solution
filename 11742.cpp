#include <algorithm>
#include <cmath>
#include <iostream>
#include <numeric>
#include <vector>

using namespace std;

struct constraint_t
{
    unsigned other;
    signed dist;

    constraint_t() : other(0), dist(0) {}
    constraint_t(unsigned o, signed d) : other(o), dist(d) {}
};

int main()
{
    unsigned n, m;

    while(cin >> n >> m && n)
    {
        vector<vector<constraint_t>> C(n);

        for(unsigned i = 0; i < m; ++i)
        {
            unsigned p1, p2;
            signed dist;

            cin >> p1 >> p2 >> dist;
            C[p1].emplace_back(constraint_t(p2, dist));
        }

        vector<unsigned> perm(n);
        iota(perm.begin(), perm.end(), 0);

        unsigned ans = 0;
        do
        {
            bool is_valid = true;

            for(signed  i = 0; i < perm.size() && is_valid; ++i)
            {
                unsigned person = perm[i];

                if (C[person].empty())
                    continue;

                for(unsigned v = 0; v < C[person].size() && is_valid; ++v)
                {
                    const auto & c = C[person][v];

                    if (c.dist > 0)
                    {
                        signed prev = i - c.dist;
                        signed next = i + c.dist + 1;

                        if (prev > 0 &&
                            any_of(&perm[0], &perm[0] + prev,
                                   [&c](unsigned t){ return t == c.other; })
                            )
                            is_valid = false;

                        if (next < n &&
                            any_of(&perm[0] + next, &perm[0] + n,
                                   [&c](unsigned t){ return t == c.other; })
                            )
                            is_valid = false;
                    }
                    else
                    {
                        for(signed j = 1; j < -c.dist && is_valid; ++j)
                        {
                            signed prev = i - j;
                            signed next = i + j;

                            if (prev >= 0 && perm[prev] == c.other)
                                is_valid = false;

                            if (next < n && perm[next] == c.other)
                                is_valid = false;
                        }
                    }
                }
            }
            if (is_valid)
                ++ans;

        } while(next_permutation(perm.begin(), perm.end()));

        cout << ans << endl;
    }
    return 0;
}
