#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    int T; cin >> T;

    while(T--) {
        int N; cin >> N;
        vector<double> listmark;
        double sum=0;
        double ave;
        double ctr=0;

        for(int i=0; i<N; i++) {
            double mark;
            cin >> mark;
            sum += mark;
            listmark.push_back(mark);
        }
        
        ave = sum/N;
        for(int i=0; i<N; i++) {
            if (listmark[i] > ave)
                ctr++;
        }

        printf("%.3f%%\n", ctr/N*100);
    }

    return 0;
}

