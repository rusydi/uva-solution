#include <algorithm>
#include <cctype>
#include <cstdio>
#include <iostream>
#include <map>
#include <string>

using namespace std;

map<string, unsigned> M;

template<typename Predicate>
bool is_valid(const string & s, unsigned pos, unsigned & sum, const string && symb, Predicate op)
{
    unsigned i = s.find(symb);
    string P = s.substr(pos, i - pos);
    sum += M[ P ];

    double temp = stod( s.substr(i + symb.size()) );
    temp *= 10;

    unsigned val = (unsigned) temp;

    return op(sum, val);
}

int main()
{
    unsigned p, g;
    cin >> p >> g;

    M.clear();

    cin.ignore();
    while(p--)
    {
        string p_name;
        double vote;
        cin >> p_name >> vote;

        vote *= 10;

        M[p_name] = (unsigned) vote;
    }

    cin.ignore();
    for(unsigned g_id = 1; g_id <= g; ++g_id)
    {
        string line;
        bool ans = false;

        getline(cin, line);
        line.erase(remove_if(line.begin(), line.end(), ::isspace), line.end());

        //parse line
        unsigned sum = 0, val = 0;
        size_t pos = 0;

        std::size_t i;
        string P;
        while(true)
        {
            i = line.find("+", pos);

            if (i == string::npos)
                break;

            P = line.substr(pos, i-pos);
            sum += M[ P ];

            pos = i + 1;
        }

        if (line.find("<=") != string::npos)
            ans = is_valid(line, pos, sum, "<=", less_equal<unsigned>());
        else if (line.find("<") != string::npos)
            ans = is_valid(line, pos, sum, "<", less<unsigned>());
        else if (line.find(">=") != string::npos)
            ans = is_valid(line, pos, sum, ">=", greater_equal<unsigned>());
        else if (line.find(">") != string::npos)
            ans = is_valid(line, pos, sum, ">", greater<unsigned>());
        else if (line.find("=") != string::npos)
            ans = is_valid(line, pos, sum, "=", equal_to<unsigned>());

        std::cout << "Guess #" << g_id << " was ";
        if (ans)
            std::cout << "correct.";
        else
            std::cout << "incorrect.";
        std::cout << std::endl;
    }

    return 0;
}
