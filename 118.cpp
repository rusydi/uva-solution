#include<array>
#include<iostream>
#include<map>
#include<string>

using namespace std;

const char V[] = {'N', 'E', 'S', 'W'};
const pair<signed, signed> A[] =
{
    make_pair(0, 1),
    make_pair(1, 0),
    make_pair(0, -1),
    make_pair(-1, 0)
};

struct Robot
{
    signed x, y, o;
    Robot(){};
};

ostream& operator<<(ostream &os, const Robot & _R)
{
    os << _R.x << " " << _R.y << " " << V[_R.o];
    return os;
}

int main()
{
    signed x, y;
    cin >> x >> y;

    array<array<bool, 51>, 51> grid;

    Robot R;
    char o;
    while(cin >> R.x >> R.y >> o)
    {
        switch(o)
        {
        case 'N': R.o = 0; break;
        case 'E': R.o = 1; break;
        case 'S': R.o = 2; break;
        case 'W': R.o = 3; break;
        }

        string inst;
        cin >> inst;
        bool is_lost = false;

        for(const auto step : inst)
        {
            if (step != 'F')
            {
                if (step == 'R')
                    R.o = (R.o + 1) % 4;
                else if (step == 'L')
                    R.o = (R.o == 0) ? 3 : R.o-1;
            }
            else
            {
                signed tx = R.x + A[R.o].first;
                signed ty = R.y + A[R.o].second;

                if (tx < 0 || tx > x || ty < 0 || ty > y)
                {
                    if (!grid[R.x][R.y])
                    {
                        grid[R.x][R.y] = true;
                        is_lost = true;
                        break;
                    }
                }
                else
                {
                    R.x = tx;
                    R.y = ty;
                }
            }
        }

        cout << R;
        if (is_lost)
            cout << " LOST";
        cout << endl;
    }
    return 0;
}
