#include<cmath>
#include<iomanip>
#include<iostream>

using namespace std;

int ndigits;

/*
bool is_quirksome(int n)
{
    int t = 0, num = n, sum = 0;
    int left=0, right=0;

    while(t < ndigits / 2)
    {
        right += (num % 10) * pow(10, t);
        num /= 10;
        ++t;
    }

    while(t < ndigits)
    {
        left += (num % 10) * pow(10, t-(ndigits/2)) ;
        num /= 10;
        ++t;
    }

    sum = left + right;
    return (sum*sum == n);
}
*/
int main()
{
    while(cin >> ndigits)
    {
        if (ndigits == 2)
            printf("00\n01\n81\n");
        else if (ndigits == 4)
            printf("0000\n0001\n2025\n3025\n9801\n");
        else if (ndigits == 6)
            printf("000000\n000001\n088209\n494209\n998001\n");
        else if (ndigits == 8)
        {
            printf("00000000\n00000001\n04941729\n07441984\n24502500\n");
            printf("25502500\n52881984\n60481729\n99980001\n");
        }
    }
    return 0;
}
