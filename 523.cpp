#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<cassert>
#include<climits>
using namespace std;

#define REP(i,n) for(int i=0, _e(n); i<_e; i++)
#define FOR(i,a,b) for(int i(a), _e(b); i<=_e; i++)
#define FORD(i,a,b) for(int i(a), _e(b); i>=_e; i--) 
#define FORIT(i, m) for (__typeof((m).begin()) i=(m).begin(); i!=(m).end(); ++i)
#define SET(t,v) memset((t), (v), sizeof(t))
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize( unique( ALL(c) ) - (c).begin() )

#define sz size()
#define pb push_back
#define VI vector<int>
#define VS vector<string>

typedef long long LL;
typedef long double LD;
typedef pair<int,int> pii;

#define D(x) if(1) cout << __LINE__ <<" "<< #x " = " << (x) << endl;
#define D2(x,y) if(1) cout << __LINE__ <<" "<< #x " = " << (x) \
     <<", " << #y " = " << (y) << endl;

struct Node {
	int at, cost;
	inline bool operator<(const Node &nextNode) const {
		if (cost != nextNode.cost) return cost > nextNode.cost;
		if (at != nextNode.at) return at > nextNode.at;
		return false;
	}
};

#define MAX_NODE 100
#define INF 99999999

int getNoOfNodes(string s) {
	int n=1;
	REP(i, s.length()) {
		if (s[i] == ' ') n++;
	}
	return n;
}

int main() {
	int w[MAX_NODE][MAX_NODE] = {{}};
	int tax[MAX_NODE] = {};

	int T; cin >> T;
	bool firstcase = true;
	cin.ignore();

	string line;
	REP(t, T) {
		if (t == 0) cin.ignore();

		SET(w, 0);
		SET(tax, 0);
		int NNodes = 0;
		int lineNum = 0;

		while(getline(cin, line) && line.length() != 0) {
			lineNum++;

			if (lineNum == 1) {
				//get number of nodes
				NNodes = getNoOfNodes(line);
			}

			if (lineNum <= NNodes) {
				//input for adjacency matrix
				stringstream ss; ss << line;
				REP(i, NNodes) {
					int _cost;
					ss >> _cost;
					w[lineNum-1][i] = _cost;
				}			
			}
			else if (lineNum == NNodes + 1) {
				//handle the tax list
				stringstream ss; ss << line;
				REP(i, NNodes) {
					int _tax;
					ss >> _tax;
					tax[i] = _tax;					
				}
			}
			else {
				/*
				 * get source and destination and compute the shortest path
				 * with the total cost
				*/
				int src, dst;
				bool visited[MAX_NODE] = {};
				int dist[MAX_NODE] = {};
				int path[MAX_NODE] = {};
				priority_queue<Node> PQ;
				stringstream ss;

				ss << line;
				ss >> src; ss >> dst;

				//to INF and beyond
				REP(i, NNodes) {
					dist[i] = INF;
				}
				dist[src-1] = 0;

				Node start; start.at = src-1; start.cost = 0;
				PQ.push(start);

				while(!PQ.empty()) {
					Node node = PQ.top(); PQ.pop();
 
					if (node.at == dst-1) break;
					if (visited[node.at]) continue;
					visited[node.at] = true;

					REP(i, NNodes) {
						if (w[node.at][i] != -1) {
							int cost = dist[node.at] + w[node.at][i];
							if (i != dst-1) cost += tax[i];

							if (cost < dist[i]) {
								dist[i] = cost;
								path[i] = node.at;

								Node nextNode; nextNode.at = i; nextNode.cost = dist[i];
								PQ.push(nextNode);
							}
						}
					}
				}

				vector<int> vpath;
				int currpath = dst-1;
				vpath.pb(currpath);

				//reconstruct the path
				while(currpath != src-1) {
					currpath = path[currpath];
					vpath.pb(currpath);
				}
				reverse(vpath.begin(), vpath.end());

				if (!firstcase) cout << endl;

				cout << "From " << src << " to " << dst << " :" << endl;
				cout << "Path: ";
				REP(i, vpath.size()) {
					if (i > 0) cout << "-->";
					cout << vpath[i]+1;
				}
				cout << endl;
				cout << "Total cost : " << dist[dst-1] << endl;
				
				firstcase = false;
			} 
		}
	}

	return 0;
}
