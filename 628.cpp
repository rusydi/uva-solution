#include<iostream>
#include<string>
#include<vector>

using namespace std;

vector<string> words;
vector<string> rules;
vector<string> output;
size_t ruleindex;
size_t wordindex;

void backtrack(size_t ri)
{

    if (ri >= rules[ruleindex].size())
    {
        for(auto o : output) cout << o;
        cout << endl;
    }
    if (rules[ruleindex][ri] == '#')
    {
        output.push_back(words[wordindex]);
        backtrack(ri+1);
        output.pop_back();
    }
    else if (rules[ruleindex][ri] == '0')
    {
        for(char c = '0'; c <= '9'; ++c)
        {
            output.emplace_back(string(1, c));
            backtrack(ri+1);
            output.pop_back();
        }
    }
}

int main()
{
    size_t n, m;

    while(cin >> n)
    {
        string word, rule;

        words.clear();
        rules.clear();
        output.clear();

        for(size_t i = 0;  i < n; ++i)
        {
            cin >> word;
            words.push_back(word);
        }

        cin >> m;
        for(size_t i =0; i < m; ++i)
        {
            cin >> rule;
            rules.push_back(rule);
        }

        cout << "--" << endl;

        for(size_t i = 0; i < m; ++i)
        {
            ruleindex = i;
            for(size_t j = 0; j < n; ++j)
            {
                wordindex = j;
                backtrack(0);
            }
        }

    }
    return 0;
}
