#include<iostream>
#include<stack>

using namespace std;

signed precedence(char a, char b)
{
    if ((a == '*' || a == '/') && (b == '+' || b == '-'))
        return 1;
    else if ((a == '+' || a == '-') && (b == '*' || b == '/'))
        return -1;
    else
        return 0;
}

void pop_print(stack<char> & S)
{
    cout << S.top();
    S.pop();
}

int main()
{
    unsigned T;
    cin >> T;
    cin.ignore(2);
    while(T--)
    {
        char c;
        stack<char> S;

        while(cin.get(c) && c != '\n')
        {
            if (::isdigit(c))
                cout << c;
            else
            {
                if (S.empty() || S.top() == '(' || c == '(')
                {
                    if (c == ')' && S.top() == '(')
                        S.pop();
                    else
                        S.push(c);
                }
                else if (c == ')')
                {
                    while(true)
                    {
                        char elem = S.top();
                        S.pop();
                        if (elem == '(')
                            break;
                        cout << elem;
                    }
                }
                else
                {
                    if (precedence(c, S.top()) > 0)
                        S.push(c);
                    else if (precedence(c, S.top()) == 0)
                    {
                        pop_print(S);
                        S.push(c);
                    }
                    else
                    {
                        do
                        {
                            pop_print(S);
                        } while(!S.empty() && precedence(c, S.top()) == -1);

                        if (!S.empty() && S.top() != '(')
                            pop_print(S);
                        S.push(c);
                    }
                }
            }
            cin.ignore();
        }
        while(!S.empty())
            pop_print(S);
        cout << endl;

        if (T) cout << endl;
    }
    return 0;
}
