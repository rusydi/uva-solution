#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <sstream>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

vector<int> original;
int table[1000][1000];
int N;

int LCS(vector<int> v) {
    memset(table, 0, sizeof(table));

    for(int i=1; i<=N; i++) {
        for(int j=1; j<=N; j++) {
            if (original[i-1] == v[j-1])
                table[i][j] = table[i-1][j-1] + 1;
            else
                table[i][j] = max(table[i][j-1], table[i-1][j]);
        }
    }
    return table[N][N];
}

int main() {
    #ifndef ONLINE_JUDGE
    freopen("in.txt","r",stdin);
    #endif
    
    string line;
    cin >> N;

    original.resize(N);

    for(int i=0; i<N; i++) {
        int temp; cin >> temp;
        original[temp-1] = i+1;
    }

    cin.ignore();
    while(getline(cin, line) && !line.empty()) {
        stringstream ss; ss << line;
        int temp;
        int t=1;
        vector<int> list; list.resize(N);

        while(ss >> temp) {
            list[temp-1] = t;
            t++;
        }

        cout << LCS(list) << endl;
    }

    return 0;
}

