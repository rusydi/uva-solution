#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    unsigned int a,b;

    while(cin >> a >> b) {
        unsigned int c = a^b;
        cout << c << endl;
    }

    return 0;
}
