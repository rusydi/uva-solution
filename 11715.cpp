#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <iomanip>
#include <cmath>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    int index;
    int counter=0;

    while(cin >> index && index) {
        double n1, n2, n3;
        double res1, res2;
        double u, v, t, s, a;

        cin >> n1 >> n2 >> n3;
        //cout << n1 << n2 << n3;

        if (index == 1) {
            u = n1; v = n2; t = n3;

            a = ( (v-u)/t );
            s = ( (v*v - u*u) / (2*a) );

            if (s < 0)
                s *= -1;

            res1 = s;
            res2 = a;
        }
        else if (index == 2) {
            u = n1; v = n2; a = n3;

            s = ( (v*v - u*u) / (2*a) );
            t = ( (v-u) / a);

            if (t < 0)
                t *= -1;
            if (s < 0)
                s *= -1;

            res1 = s;
            res2 = t;
        }
        else if (index == 3) {
            u = n1; a = n2; s = n3;

            v = sqrt(2*a*s + u*u);
            t = ((v-u)/a);

            if (t < 0)
                t *= -1;

            res1 = v;
            res2 = t;
        }
        else if (index == 4) {
            v = n1; a = n2; s = n3;

            u = sqrt(v*v - 2*a*s);
            t = ((v-u) / a);

            if (t < 0)
                t *= -1;

            res1 = u;
            res2 = t;
        }

        cout << "Case " << counter+1 << ": ";
        cout << setprecision(3) << setfill('0') << fixed << res1 << " " << res2 << endl;

        counter++;
    }

    return 0;
}
