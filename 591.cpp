#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    int N;
    int t=0;

    while(cin >> N && N) {
        int sum=0, ans=0;
        vector<int> box;

        for(int i=0; i<N; i++) {
            int temp;
            cin >> temp;
            box.push_back(temp);
            
            sum += temp;
        }

        for(int i=0; i<N; i++) {
            if (box[i] > (sum/N))
                ans += (box[i] - sum/N);
        }

        cout << "Set #" << t+1 << endl;
        cout << "The minimum number of moves is " << ans << "." << endl;
        cout << endl;
        t++;
    }

    return 0;
}

