#include <algorithm>
#include<iostream>
#include<string>

using namespace std;

const unsigned maxnum = 98765;
const unsigned minnum = 1234;
const size_t len = 10;

bool is_distinct(const string & si, const string & sj)
{
    string s = si + sj;

    sort(s.begin(), s.end());
    s.erase(unique(s.begin(), s.end()), s.end());

    return s.size() == len;
}

int main()
{
    unsigned N;
    bool case1 = true;

    while(cin >> N && N)
    {
        if (!case1)
            cout << endl;

        bool found = false;
        for(unsigned i = minnum; i <= (maxnum / N) + 1; ++i)
        {
            unsigned j = i * N;

            string si = to_string(i);
            string sj = to_string(j);

            if (si.size() == 4)
                si = "0" + si;

            if (j <= maxnum && is_distinct(si, sj))
            {
                cout << sj << " / " << si << " = " << N << endl;
                found = true;
            }
        }
        if (!found)
            cout << "There are no solutions for " << N << "." << endl;

        case1 = false;
    }
    return 0;
}
