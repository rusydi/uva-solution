#include<iostream>
#include<cstring>
using namespace std;

#define MAX_SIZE 30000

long long ans[30005];

void init() {
        memset(ans, 0, sizeof(ans));
        int coin[] = {1, 5, 10, 25, 50};

        for(int i=0; i<5; i++) {
                ans[coin[i]]++;
                for(int j=1; j<MAX_SIZE; j++) {
                        if (ans[j] >= 1) {
                                if (j + coin[i] <= MAX_SIZE)
                                        ans[j + coin[i]] += ans[j];
                        }
                }
        }
}

int main() {
        int N;

        init();

        while(cin >> N) {
                if (ans[N] == 1 || N == 0) {
                        cout << "There is only 1 way to produce " << N << " cents change." << endl;
                }
                else {
                        cout << "There are " << ans[N] << " ways to produce " << N << " cents change." << endl;
                }
        }
}

