#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <vector>
#include <algorithm>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

#define PSI pair<string, int>

int main() {
    int tcase; cin >> tcase;

    for(int q=0; q<tcase; q++) {
        int nt;
        map<string, int> M; M.clear();

        cin >> nt;

        while(nt--) {
            string str;
            string strtemp; strtemp.clear();
            int idx=0;
            
            cin >> str;

            for(int i=0; i<str.length(); i++) {
                if (idx == 3) {
                    strtemp.push_back('-');
                    idx++;
                }

                if (idx != 3) {
                    if (str[i] >= '0' && str[i] <= '9') {
                        strtemp.push_back(str[i]);
                        idx++;
                    }
                    else if (isalpha(str[i])) {
                        if (str[i] >= 'A' && str[i] <= 'C')
                            strtemp.push_back('2');
                        else if (str[i] >= 'D' && str[i] <= 'F')
                            strtemp.push_back('3');
                        else if (str[i] >= 'G' && str[i] <= 'I')
                            strtemp.push_back('4');
                        else if (str[i] >= 'J' && str[i] <= 'L')
                            strtemp.push_back('5');
                        else if (str[i] >= 'M' && str[i] <= 'O')
                            strtemp.push_back('6');
                        else if (str[i] >= 'P' && str[i] <= 'S')
                            strtemp.push_back('7');
                        else if (str[i] >= 'T' && str[i] <= 'V')
                            strtemp.push_back('8');
                        else if (str[i] >= 'W' && str[i] <= 'Z')
                            strtemp.push_back('9');
                        idx++;
                    }
                    else
                        continue;
                }
            }
            M[strtemp]++;
        }
        
        vector<PSI> V(M.begin(), M.end());
        bool found=false;
        
        if (q > 0)
            cout << endl;

        for(int i=0; i<V.size(); i++) {
            if (V[i].second > 1) {
                found = true;
                cout << V[i].first << " " << V[i].second << endl;
            }                
        }

        if (!found) {
            cout << "No duplicates." << endl;
        }
    }
    return 0;
}
