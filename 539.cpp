
#include<cstdio>
#include<cstring>
#include<iostream>
#include<vector>

using namespace std;

vector<vector<int> > G;
bool visited_edge[25][25];
int ans;
int edgecount;

void search(int node)
{
    ans = edgecount > ans ? edgecount : ans;

    for(auto adjnode : G[node])
    {
        if (visited_edge[node][adjnode] || visited_edge[adjnode][node]) continue;

        visited_edge[node][adjnode] = true;
        visited_edge[adjnode][node] = true;
        edgecount++;
        search(adjnode);
        edgecount--;
        visited_edge[node][adjnode] = false;
        visited_edge[adjnode][node] = false;
    }
}

int main()
{
    int n, m;
    while(cin >> n >> m && n && m)
    {

        ans = 0;
        memset(visited_edge, 0, sizeof(bool)*25*25);
        G.clear();
        G.resize(n);
        for(int i=0; i < m; ++i)
        {
            int node1, node2;
            cin >> node1 >> node2;

            G[node1].push_back(node2);
            G[node2].push_back(node1);
        }

        for(int i=0; i < n; ++i)
        {
            edgecount = 0;
            search(i);
        }
        cout << ans << endl;
    }
    return 0;
}
