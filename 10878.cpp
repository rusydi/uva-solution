#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <bitset>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

void convert(string str) {
    for(int i=0; i<str.length(); i++) {
        if (str[i] == ' ')
            str[i] = '0';
        else if (str[i] == 'o')
            str[i] = '1';
    }

    bitset<8> B(str);
    cout << (char) B.to_ulong();
}

int main() {
    string line;

    while(getline(cin, line)) {
        string str; str.clear();

        if (line == "___________")
            continue;

        for(int i=1; i<=9; i++) {
            if (line[i] == ' ' || line[i] == 'o')
                str.push_back(line[i]);
        }

        convert(str);
    }

    return 0;
}
