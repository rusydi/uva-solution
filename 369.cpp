#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<cassert>
#include<climits>
using namespace std;

#define REP(i,n) for(int i=0, _e(n); i<_e; i++)
#define FOR(i,a,b) for(int i(a), _e(b); i<=_e; i++)
#define FORD(i,a,b) for(int i(a), _e(b); i>=_e; i--) 
#define FORIT(i, m) for (__typeof((m).begin()) i=(m).begin(); i!=(m).end(); ++i)
#define SET(t,v) memset((t), (v), sizeof(t))
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize( unique( ALL(c) ) - (c).begin() )

#define sz size()
#define pb push_back
#define VI vector<int>
#define VS vector<string>

typedef long long LL;
typedef long double LD;
typedef pair<int,int> pii;

#define D(x) if(1) cout << __LINE__ <<" "<< #x " = " << (x) << endl;
#define D2(x,y) if(1) cout << __LINE__ <<" "<< #x " = " << (x) \
     <<", " << #y " = " << (y) << endl;

int d[101][101] = {{}};

int C(int N, int M) {
	if (d[N][M] == -1) d[N][M] = C(N-1, M-1) + C(N-1, M);
	return d[N][M];
}

int main() {
	int N, M;

	//initialize d[]
	memset(d, -1, sizeof(d));
	REP(i, 101) d[i][0] = 1;
	FOR(i, 1, 100) d[0][i] = 0;

	while(scanf("%d %d", &N, &M) == 2 && (N | M)) {
		printf("%d things taken %d at a time is %d exactly.\n", N, M, C(N, M));
	}
	return 0;
}
