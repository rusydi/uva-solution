#include<array>
#include<iostream>
#include<numeric>

using namespace std;

long long s, d;
long long maxsum;
array<int, 12> report;

void backtrack(int depth)
{
    if (depth >= 5 &&
            accumulate(report.begin()+(depth-5) , report.begin()+depth, 0) > 0)
        return;

    if (depth == 12)
    {
        int temp = accumulate(report.begin(), report.end(), 0);
        maxsum = temp > maxsum ? temp : maxsum;
        return;
    }

    report[depth] = s;
    backtrack(depth+1);

    report[depth] = d;
    backtrack(depth+1);
}

int main()
{
    while(cin >> s >> d)
    {
        maxsum = 0;
        d *= -1;
        backtrack(0);

        if (maxsum == 0)
            cout << "Deficit"<< endl;
        else
            cout << maxsum << endl;
    }
    return 0;
}
