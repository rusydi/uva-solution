#include<algorithm>
#include<iostream>
#include<queue>
#include<set>

using namespace std;

int main()
{
    int N;
    int t = 0;
    while(cin >> N && N)
    {
        queue< vector<int> > Q;
        set< vector<int> > visited;
        vector<int> v;
        int counter = 0;

        v.resize(N);
        for(int i = 0; i < N; ++i)
            cin >> v[i];

        if (!is_sorted(v.begin(), v.end()))
            Q.push(v);

        while(!Q.empty())
        {
            vector<int> current = Q.front();
            visited.insert(current);
            Q.pop();

            if (is_sorted(current.begin(), current.end()))
            {
                counter++;
                while(!Q.empty())
                {
                    current = Q.front();
                    Q.pop();
                    counter = is_sorted(current.begin(), current.end()) ?
                                counter + 1 : counter;
                }
                break;
            }

            for(int i = 0; i < N-1; ++i)
            {
                vector<int> temp(current);
                swap(temp[i], temp[i+1]);
                if (!visited.count(temp))
                    Q.push(temp);
            }
        }
        printf("There are %d swap maps for input data set %d.\n", counter, ++t);
    }
}
