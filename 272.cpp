#include <iostream>
#include <string>

using namespace std;

int main(int argc, char** argv) {

    string str;
    int stat = 0;

    while(getline(cin, str)) {
        for(int i=0; i<str.length(); i++) {
            if ((int)str[i] == 34) {
                if (stat == 0)
                    cout << "``";
                else if(stat == 1)
                    cout << "''";
                stat = (stat + 1) % 2;
            }
            else
                cout << str[i];
        }
        cout << endl;
    }

    return 0;
}

