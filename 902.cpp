#include <stdlib.h>
#include <iostream>
#include <map>

using namespace std;

int main(int argc, char** argv) {
    int n;
    string msg;

    while (cin >> n >> msg) {
        map<string, int> M;
        map<string, int>::iterator it;
        pair<string,int> highest;


        for (int i=0; i<msg.length() - n; i++) {
            M[msg.substr(i, n)]++;
        }

        it = M.begin();

        highest = *M.begin();

        do {
            if ((*it).second > highest.second)
                highest = (*it);
        } while ( M.value_comp()(*it++, *M.rbegin()));

        cout<< highest.first <<endl;

    }

    return (EXIT_SUCCESS);
}

