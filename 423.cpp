#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<cassert>
#include<climits>
using namespace std;

#define REP(i,n) for(int i=0, _e(n); i<_e; i++)
#define FOR(i,a,b) for(int i(a), _e(b); i<=_e; i++)
#define FORD(i,a,b) for(int i(a), _e(b); i>=_e; i--) 
#define FORIT(i, m) for (__typeof((m).begin()) i=(m).begin(); i!=(m).end(); ++i)
#define SET(t,v) memset((t), (v), sizeof(t))
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize( unique( ALL(c) ) - (c).begin() )

#define sz size()
#define pb push_back
#define VI vector<int>
#define VS vector<string>

typedef long long LL;
typedef long double LD;
typedef pair<int,int> pii;

#define D(x) if(1) cout << __LINE__ <<" "<< #x " = " << (x) << endl;
#define D2(x,y) if(1) cout << __LINE__ <<" "<< #x " = " << (x) \
     <<", " << #y " = " << (y) << endl;

struct Node {
	unsigned long long at;
	unsigned long long cost;

	inline bool operator<(const Node &nextNode) const {
		if (cost != nextNode.cost) return cost > nextNode.cost;
		if (at != nextNode.at) return at > nextNode.at;
		return false;
	}
};

unsigned long long str2num(string s) {
	reverse(s.begin(), s.end());
	unsigned long long n = 0;

	REP(i, s.length()) {
		n += (s[i] - '0') * pow(10, i);
	}

	return n;
}

int main() {
	unsigned long long w[101][101] = {{}};
	bool visited[101] = {};
	unsigned long long dist[101] = {};
	priority_queue<Node> PQ;

	int N; cin >> N;

	FOR(i, 1, N-1) {
		REP(j, i) {
			string weight; cin >> weight;
			if (weight != "x") w[i][j] = w[j][i] = str2num(weight);
		}
	}

	//to INF and beyond
	FOR(i, 1, N-1) {
		dist[i] = ULONG_MAX;
	}

	Node start; start.at = 0; start.cost = 0;
	PQ.push(start);

	while(!PQ.empty()) {
		Node node  = PQ.top(); PQ.pop();

		if (visited[node.at]) continue;
		visited[node.at] = true;

		REP(i, N) {
			if (w[node.at][i] != 0) {
				unsigned long long cost = dist[node.at] + w[node.at][i];
				if (cost < dist[i]) {
					dist[i] = cost;
					Node newNode; newNode.at = i; newNode.cost = dist[i];
					PQ.push(newNode);
				}
			}
		}
	}

	unsigned long long maxTime = 0;

	REP(i, N) {
		if (dist[i] > maxTime) maxTime = dist[i];
	}

	cout << maxTime << endl;

	return 0;
}
