#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

#define MAX 32768

bool isnprime[MAX];
vector<int> prime;

void sieve() {
    isnprime[0] = isnprime[1] = true;

    for(int i=2; i<MAX; i++) {
        if (!isnprime[i]) {
            for(int j=i+i; j<MAX; j+=i)
                isnprime[j] = true;
            prime.push_back(i);
        }
    }
}


int main() {
    sieve();
    int N;

    while(cin >> N && N) {
        int i=0;
        int ans=0;

        for(int i=0; prime[i] <= N/2; i++) {
            if (binary_search(prime.begin(), prime.end(), N-prime[i]))
                ans++;
        }
        cout << ans << endl;
    }
    return 0;
}

