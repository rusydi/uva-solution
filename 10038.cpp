#include <iostream>
#include <string>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

typedef long long LL;

bool checklist[3005];

int main(int argc, char** argv) {
    LL num;

    while(cin >> num) {
        bool stat = true;
        LL prev_num;

        memset(&checklist, 0, sizeof(checklist));

        cin >> prev_num;

        for(int i=1; i<num; i++) {
            int n;
            cin >> n;
            checklist[abs(n - prev_num)] = true;
            prev_num = n;
        }

        for(int i=1; i<=num-1; i++) {
            if (!checklist[i]) {
                stat = false;
                break;
            }
        }

        if (stat || num == 1)
            cout << "Jolly" << endl;
        else
            cout << "Not jolly" << endl;
    }

    return 0;
}
