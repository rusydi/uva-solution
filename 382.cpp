#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<cassert>
#include<climits>
using namespace std;

#define REP(i,n) for(int i=0, _e(n); i<_e; i++)
#define FOR(i,a,b) for(int i(a), _e(b); i<=_e; i++)
#define FORD(i,a,b) for(int i(a), _e(b); i>=_e; i--) 
#define FORIT(i, m) for (__typeof((m).begin()) i=(m).begin(); i!=(m).end(); ++i)
#define SET(t,v) memset((t), (v), sizeof(t))
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize( unique( ALL(c) ) - (c).begin() )

#define sz size()
#define pb push_back
#define VI vector<int>
#define VS vector<string>

typedef long long LL;
typedef long double LD;
typedef pair<int,int> pii;

#define D(x) if(1) cout << __LINE__ <<" "<< #x " = " << (x) << endl;
#define D2(x,y) if(1) cout << __LINE__ <<" "<< #x " = " << (x) \
     <<", " << #y " = " << (y) << endl;

int main() {
	int N;
	printf("PERFECTION OUTPUT\n");
	while(scanf("%d", &N) == 1 && N) {

		int sumfactor = 1;
		bool factored[60001] = {};

		FOR(i, 2, 245) {
			if (i >= N) break;
			if (N % i == 0 && !factored[i]) {
				if (i == N/i) sumfactor += i;
				else sumfactor += (i + N/i);
				factored[i] = factored[N/i] = true;
			}
		}
		printf("%5d  ", N);


		if (sumfactor < N || N == 1) printf("DEFICIENT\n");
		else if (sumfactor > N) printf("ABUNDANT\n");
		else printf("PERFECT\n");
	}
	printf("END OF OUTPUT\n");
	return 0;
}
