#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iomanip>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    long double p, n;

    while(cin >> n >> p) {
        long double res = exp(log(p)/n);
        cout << setprecision(0) << fixed << res << endl;
    }

    return 0;
}
