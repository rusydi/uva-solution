#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <sstream>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int ctr=0;
vector<string> G;
bool visited[100][100];

void DFS(int x, int y) {
    if (x < 0 || y < 0 || x >= G[0].length() || y >= G[0].length() ||
            visited[x][y] || G[x][y] == 'L')
        return;

    visited[x][y] = true;
    ctr++;

    DFS(x, y+1); DFS(x, y-1); DFS(x+1, y); DFS(x-1, y);
    DFS(x-1, y+1); DFS(x-1, y-1); DFS(x+1, y+1); DFS(x+1, y-1);
}

int main() {
    freopen("in.txt","r",stdin);

    int T; cin >> T;

    for(int t=0; t<T; t++) {
        if (t > 0) cout << endl;
        string str, line;

        G.clear(); // clear the graph

        cin >> str;
        G.push_back(str);

        for(int i1=1; i1<str.length(); i1++) {
            cin >> str;
            G.push_back(str);
        }
        
        cin.ignore();
        
        while(getline(cin, line) && !line.empty()) {
            int x, y;
            stringstream ss; ss << line;
            ctr = 0;

            ss >> x; ss >> y;

            memset(visited, 0, sizeof(visited));
            DFS(x-1, y-1);
            
            cout << ctr << endl;
        }
    }

    return 0;
}

