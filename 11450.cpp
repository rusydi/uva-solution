#include<cstring>
#include<iostream>
#include<limits>
#include<vector>

using namespace std;

signed M, C;
signed state[201][21];
vector<vector<signed>> garments;

signed findsol(signed money, signed gid)
{
    if (money < 0) return numeric_limits<signed>::min();
    if (gid == C) return M - money;
    if (state[money][gid] != -1) return state[money][gid];

    signed ret = numeric_limits<signed>::min();
    for(signed i=0; i < garments[gid].size(); ++i)
        ret = max(ret, findsol(money - garments[gid][i], gid+1));
    state[money][gid] = ret;
    return ret;
}

int main()
{
    signed N;
    cin >> N;

    while(N--)
    {
        signed ans=numeric_limits<signed>::min();

        memset(state, -1, sizeof(signed)*201*21);
        cin >> M >> C;

        garments.clear();
        garments.resize(C);

        for(signed i=0; i < C; ++i)
        {
            vector<signed> v;
            signed ntype;

            cin >> ntype;
            v.resize(ntype);

            for(signed j=0; j < ntype; ++j)
            {
                cin >> v[j];
            }
            garments[i] = v;
        }

        ans = findsol(M, 0);

        if (ans != numeric_limits<signed>::min())
            cout << ans << endl;
        else
            cout << "no solution" << endl;
    }

    return 0;
}
