#include<algorithm>
#include<cstring>
#include<iostream>
#include<numeric>
#include<vector>

using namespace std;

long long num[5];
bool is_possible;
long long sum;

void search(int index)
{
    if (index == 5 && sum == 23)
        is_possible = true;

    if (index < 5)
    {
        sum = sum * num[index];
        search(index+1);
        sum = sum / num[index];

        sum = sum + num[index];
        search(index+1);
        sum = sum - num[index];

        sum = sum - num[index];
        search(index+1);
        sum = sum + num[index];
    }
}

int main()
{
    while(true)
    {
        sum = 0;
        is_possible = false;
        memset(num, 0, sizeof(long long)*5);
        cin >> num[0] >> num[1] >> num[2] >> num[3] >> num[4];

        if (!accumulate(num, num+5, 0)) break;
        sort(num, num+5);

        do {
            sum = num[0];
            search(1);
            if (is_possible) break;
        } while(next_permutation(num,num+5));

        if (is_possible) printf("Possible\n");
        else printf("Impossible\n");
    }
    return 0;
}
