#include<iostream>
#include<unordered_set>

using namespace std;

int main()
{
    unsigned N, M;
    while(cin >> N >> M && N && M)
    {
        unordered_set<unsigned> S;

        unsigned n, ans=0;
        for(size_t i = 0; i < N; cin >> n, S.insert(n), ++i);
        for(size_t i = 0; i < M; ++i)
        {
            cin >> n;
            if (S.count(n))
                ans++;
        }
        cout << ans << endl;
    }
    return 0;
}
