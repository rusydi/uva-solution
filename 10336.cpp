#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

char G[1000][1000];
bool visited[1000][1000];
int R, C; 
int nodectr=0;

void DFS(int i, int j, char c) {
    if (i < 0 || j < 0 || i >= R || j >= C || visited[i][j] || G[i][j] != c)
        return;

    visited[i][j] = true;
    nodectr++;

    DFS(i+1, j, c); DFS(i-1, j, c);
    DFS(i, j+1, c); DFS(i, j-1, c);
}

bool comp(pair<int, char> A, pair<int, char> B) {
    if (A.first == B.first)
        return (A.second < B.second);
    else
        return (A.first > B.first);
}

int main() {
    int T; cin >> T;
    for(int t=0; t<T; t++) {
        nodectr=0;
        vector<pair<int, char> > V;
        cin >> R >> C;
        bool stop=false;

        memset(G, 0, sizeof(G));
        memset(visited, 0, sizeof(visited));

        V.resize(26);
        for(int i=0; i<26; i++) {
            V[i].first = 0;
            V[i].second = (char) (i + 97);
        }

        for(int i=0; i<R; i++) {
            for(int j=0; j<C; j++)
                cin >> G[i][j];
        }

        for(int i=0; i<R; i++) {
            for(int j=0; j<C; j++) {
                if (!visited[i][j]) {
                    DFS(i, j, G[i][j]);

                    V[G[i][j] - 'a'].first++;

                    if (nodectr == R*C) {
                        stop = true;
                        break;
                    }
                }
            }
            if (stop) break;
        }

        sort(V.begin(), V.end(), comp);

        cout << "World #" << t+1 << endl;
        for(int i=0; i<V.size(); i++) {
            if (V[i].first == 0) break;
            cout << V[i].second << ": " << V[i].first << endl;
        }
    }

    return 0;
}

