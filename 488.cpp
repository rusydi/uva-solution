#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int main() {
    int tcase; cin >> tcase;

    while(tcase--) {
        int A, F;

        cin >> A >> F;

        for(int i=1; i<=F; i++) {
            for(int j=1; j<=A; j++) {
                for(int k=1; k<=j; k++)
                    cout << j;
                cout << endl;
            }

            for(int j=A-1; j>=1; j--) {
                for(int k=j; k>=1; k--)
                    cout << j;
                cout << endl;
            }

            if (i == F && tcase==0)
                break;
            cout << endl;
        }
    }

    return 0;
}

