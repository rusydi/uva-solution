#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>

using namespace std;

vector<int> pow2;

void init() {
    int n=1;
    pow2.push_back(0); pow2.push_back(1);
   
    while( (n = 2*n) <= 16384) {
        pow2.push_back(n);
    }
}

int main() {
    init();
   
    int N;
    int t=0;
   
    while(cin >> N && N != -1) {
        for(int i=0; i<pow2.size()-1; i++) {
            if (pow2[i] < N && pow2[i+1] >= N ) {
                cout << "Case " << t+1 << ": " << i << endl;
                t++;
                break;
            }
        }
    }

    return 0;
}


