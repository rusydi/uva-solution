#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<cassert>
#include<climits>
using namespace std;

#define REP(i,n) for(int i=0, _e(n); i<_e; i++)
#define FOR(i,a,b) for(int i(a), _e(b); i<=_e; i++)
#define FORD(i,a,b) for(int i(a), _e(b); i>=_e; i--) 
#define FORIT(i, m) for (__typeof((m).begin()) i=(m).begin(); i!=(m).end(); ++i)
#define SET(t,v) memset((t), (v), sizeof(t))
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize( unique( ALL(c) ) - (c).begin() )

#define sz size()
#define pb push_back
#define VI vector<int>
#define VS vector<string>

typedef long long LL;
typedef long double LD;
typedef pair<int,int> pii;

#define D(x) if(1) cout << __LINE__ <<" "<< #x " = " << (x) << endl;
#define D2(x,y) if(1) cout << __LINE__ <<" "<< #x " = " << (x) \
     <<", " << #y " = " << (y) << endl;

struct Node {
	int at, cost;
	inline bool operator<(const Node &nextnode) const {
		if (cost != nextnode.cost) return cost > nextnode.cost;
		if (at != nextnode.at) return at > nextnode.at;
		return false;
	}
};

int main() {
	int NI; int t=0;

	while((scanf("%d", &NI) == 1) && NI) {
		vector< vector<Node> > G(NI);
		priority_queue<Node> PQ;
		bool visited[100] = {};
		int dist[100] = {};
		int path[100] = {};
		int src, dst;

		REP(i, NI) {
			int N; cin >> N;
			REP(j, N) {
				int at, cost;
				Node node;
				scanf("%d %d", &at, &cost);
				node.at = at; node.cost = cost;
				G[i].pb(node);
			}
		}
		scanf("%d %d", &src, &dst);

		//to INF and beyond
		REP(i, NI) dist[i] = INT_MAX;
		dist[src-1] = 0;
		Node start; start.at = src; start.cost = 0;
		PQ.push(start);

		while(!PQ.empty()) {
			Node node = PQ.top(); PQ.pop();

			if (node.at == dst) break;
			if (visited[node.at-1]) continue;
			visited[node.at-1] = true;

			FOR(i, 1, G[node.at-1].size()) {
				int cost =  dist[node.at-1] + G[node.at-1][i-1].cost;
				if (cost < dist[G[node.at-1][i-1].at-1]) {
					dist[G[node.at-1][i-1].at-1] = cost;
					path[G[node.at-1][i-1].at-1] = node.at;
					Node nextnode = G[node.at-1][i-1]; nextnode.cost = cost;
					PQ.push(nextnode);
				}
			}
		}
		
		//get path
		vector<int> vpath;
		vpath.pb(dst);
		int currpath = dst;

		while(currpath != src) {
			currpath = path[currpath-1];
			vpath.pb(currpath);
		}
		reverse(vpath.begin(), vpath.end());

		printf("Case %d: Path = ", ++t);
		REP(i, vpath.size()) {
			if (i > 0) printf(" ");
			printf("%d", vpath[i]);
		}
		printf("; %d second delay\n", dist[dst-1]);
	}
	return 0;
}
