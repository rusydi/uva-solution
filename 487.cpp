/*
    There is a mistake in the problem description.
    The first line gives the number of test cases.
    The output from each test case must be separated by a blank line.
*/

#include<algorithm>
#include<cstring>
#include<iostream>
#include<string>
#include<vector>

using namespace std;

int n;
char G[20][20];
vector<string> words;
string path;

void search(int x, int y)
{
    if (path.size() >= 3) words.push_back(path);

    if (y + 1 < n && path.back() < G[x][y+1])
    {
        path.push_back(G[x][y+1]);
        search(x, y+1);
        path.pop_back();
    }
    if (y - 1 >= 0 && path.back() < G[x][y-1])
    {
        path.push_back(G[x][y-1]);
        search(x, y-1);
        path.pop_back();
    }
    if (x + 1 < n && path.back() < G[x+1][y])
    {
        path.push_back(G[x+1][y]);
        search(x+1, y);
        path.pop_back();
    }
    if (x + 1 < n && y + 1 < n && path.back() < G[x+1][y+1])
    {
        path.push_back(G[x+1][y+1]);
        search(x+1, y+1);
        path.pop_back();
    }
    if (x + 1 < n && y - 1 >= 0 && path.back() < G[x+1][y-1])
    {
        path.push_back(G[x+1][y-1]);
        search(x+1, y-1);
        path.pop_back();
    }
    if (x - 1 >= 0 && path.back() < G[x-1][y])
    {
        path.push_back(G[x-1][y]);
        search(x-1, y);
        path.pop_back();
    }
    if (x - 1 >= 0 && y + 1 < n && path.back() < G[x-1][y+1])
    {
        path.push_back(G[x-1][y+1]);
        search(x-1, y+1);
        path.pop_back();
    }
    if (x - 1 >= 0 && y - 1 >= 0 && path.back() < G[x-1][y-1])
    {
        path.push_back(G[x-1][y-1]);
        search(x-1, y-1);
        path.pop_back();
    }
    return;
}

int main()
{
    int T; cin >> T;
    while(T--)
    {
        memset(G, 0, sizeof(char)*20*20);
        words.clear();

        cin >> n;
        for(int i = 0; i < n; ++i)
            for(int j = 0; j < n; ++j)
                cin >> G[i][j];

        for(int i = 0; i < n; ++i)
            for(int j = 0; j < n; ++j)
            {
                path.clear();
                path.push_back(G[i][j]);
                search(i, j);
            }

        sort(words.begin(), words.end(), [](string l, string r){
            return l.size() != r.size() ? l.size() < r.size() : l < r;
        });
        words.erase(unique(words.begin(), words.end()), words.end());
        for(auto word : words) cout << word << endl;
        if (T) cout << endl;
    }

    return 0;
}
