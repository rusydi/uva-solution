#include<algorithm>
#include<iostream>
#include<vector>

using namespace std;

struct entry_t
{
    unsigned i, j;
    signed value;
    entry_t() : i(0), j(0), value(0) {}
    entry_t(unsigned _i, unsigned _j) : i(_i), j(_j) {}
};

int main()
{
    unsigned nrows, ncols;
    while(cin >> nrows >> ncols)
    {
        vector<entry_t> entries;
        vector< vector<pair<unsigned, signed>> > adjlist(ncols+1);
        size_t index = entries.size();

        for(unsigned i = 1; i <= nrows; ++i)
        {
            unsigned nelems;
            cin >> nelems;

            if (nelems == 0) continue;

            for(unsigned t = 1; t <= nelems; ++t)
            {
                unsigned j; cin >> j;
                entries.emplace_back(j, i);
            }

            for(unsigned t = 1; t <= nelems; ++t)
            {
                cin >> entries[index].value;
                index++;
            }
        }

        while(!entries.empty())
        {
            entry_t e = entries.back();
            entries.pop_back();
            adjlist[e.i].emplace_back(e.j, e.value);
        }

        for(auto & v : adjlist)
            sort(v.begin(), v.end());

        cout << ncols << " " << nrows << endl;

        for(size_t i = 1; i < adjlist.size(); ++i)
        {
            cout << adjlist[i].size();
            for(const auto & e : adjlist[i])
                cout << " " << e.first;
            cout << endl;
            for(size_t j = 0; j < adjlist[i].size(); ++j)
            {
                if (j) cout << " ";
                cout << adjlist[i][j].second;
            }
            cout << endl;
        }
    }
    return 0;
}
