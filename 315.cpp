#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <sstream>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

bool G[101][101];
bool visited[101];
int Nnode;
int nodectr;

void DFS(int start) {
    if (visited[start])
        return;

    visited[start] = true;
    nodectr++;

    for(int i1=0; i1<Nnode; i1++) {
        if (G[start][i1]) {
            DFS(i1);
        }
    }
}

int main() {
    while(cin >> Nnode && Nnode) {
        string line;
        int ans=0;

        memset(G, 0, sizeof(G));
        cin.ignore();
        
        while(getline(cin, line)) {
            stringstream ss;
            int srcnode;
            int destnode;
            
            ss << line;
            ss >> srcnode;
            
            if (srcnode == 0) break;
            else {
                while(ss >> destnode) {
                    G[srcnode-1][destnode-1] = 1;
                    G[destnode-1][srcnode-1] = 1;
                }
            }
        }
        
        for(int node=0; node<Nnode; node++) {
            memset(visited, 0, sizeof(visited));
            nodectr=0;
            visited[node] = true;

            DFS( (node +1) % Nnode );

            if (nodectr != (Nnode-1) )
                ans++;
        }
        cout << ans << endl;
    }
    return 0;
}

