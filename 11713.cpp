#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <string>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

bool isvocal(char c) {
    if (c == 'a' || c == 'i' || c == 'u' || c == 'e' || c == 'o')
        return true;
    else
        return false;
}

int main() {
    int tcase; cin >> tcase;

    while(tcase--) {
        string name1, name2;
        bool stat=true;

        cin >> name1 >> name2;
    
        if (name1.length() == name2.length()) {
            for(int i=0; i<name1.length(); i++) {
                if (!isvocal(name1[i])) {
                    if (name1[i] != name2[i]) {
                        stat = false;
                        break;
                    }

                }
            }
        }
        else
            stat=false;

        if (stat)
            cout << "Yes";
        else
            cout << "No";
        cout << endl;
    }

    return 0;
}
