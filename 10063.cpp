#include<iostream>
#include<string>

using namespace std;

string L;
string str;

void backtrack(int index)
{
    if (L.size() == str.size())
    {
        cout << L << endl;
        return;
    }
    else
    {
        for(auto it = L.begin(); it != L.end(); ++it)
        {
            L.insert(it, str[index]);
            backtrack(index+1);
            L.erase(it);
        }
        L.push_back(str[index]);
        backtrack(index+1);
        L.pop_back();
    }
}

int main()
{
    int T = 0;
    while(cin >> str)
    {
        L.clear();
        L.push_back(str[0]);
        if (T) cout << endl;
        backtrack(1);
        ++T;
    }
    return 0;
}
