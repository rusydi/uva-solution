#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>

using namespace std;

typedef unsigned long long ULL;
typedef long long LL;

int f91(int N) {
    if (N >= 101)
        return N-10;
    else if (N <= 100)
        return f91(f91(N+11));
}

int main() {
    int N;
    while(cin >> N && N)
        cout << "f91(" << N << ") = " << f91(N) << endl;

    return 0;
}

