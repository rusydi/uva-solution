#include<cstdio>
#include<sstream>
#include<cstdlib>
#include<cctype>
#include<cmath>
#include<algorithm>
#include<set>
#include<queue>
#include<stack>
#include<list>
#include<iostream>
#include<string>
#include<vector>
#include<cstring>
#include<map>
#include<cassert>
#include<climits>
using namespace std;

#define REP(i,n) for(int i=0, _e(n); i<_e; i++)
#define FOR(i,a,b) for(int i(a), _e(b); i<=_e; i++)
#define FORD(i,a,b) for(int i(a), _e(b); i>=_e; i--) 
#define FORIT(i, m) for (__typeof((m).begin()) i=(m).begin(); i!=(m).end(); ++i)
#define SET(t,v) memset((t), (v), sizeof(t))
#define ALL(x) x.begin(), x.end()
#define UNIQUE(c) (c).resize( unique( ALL(c) ) - (c).begin() )

#define sz size()
#define pb push_back
#define VI vector<int>
#define VS vector<string>

typedef long long LL;
typedef long double LD;
typedef pair<int,int> pii;

#define D(x) if(1) cout << __LINE__ <<" "<< #x " = " << (x) << endl;
#define D2(x,y) if(1) cout << __LINE__ <<" "<< #x " = " << (x) \
     <<", " << #y " = " << (y) << endl;

int main() {
	stringstream ss;
	string s;
	map<string, int> M;
	vector<string> listwords;

	while(getline(cin, s) && s != "#") {
		ss.clear();
		ss << s;

		string word;
		while(ss >> word) {
			listwords.pb(word);
			string tempword = word;
			REP(i, tempword.length()) tempword[i] = tolower(tempword[i]);
			sort(ALL(tempword));

			if (M[tempword] >= 1) M[tempword]++;
			else M[tempword] = 1;
		}
	}
	sort(ALL(listwords));
	REP(i, listwords.sz) {
		string tempword = listwords[i];
		REP(j, tempword.length()) tempword[j] = tolower(tempword[j]);
		sort(ALL(tempword));

		if  (M[tempword] == 1) cout << listwords[i] << endl;
	}

	return 0;
}
